﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aurora
{
    enum CellFace
    {
        PlusX,
        PlusY,
        MinusX,
        MinusY
    }
}

﻿using MathNet.Numerics.LinearAlgebra;
using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.IO;

namespace Aurora
{
    class EventCollector
    {
        public struct EventData
        {
            public double ClockTime;
            public Vector<double> Location;
            public string Species;
        }

        public ConcurrentBag<EventData> EventDataBag;
        readonly string Filename;

        public EventCollector(string Filename)
        {
            EventDataBag = new ConcurrentBag<EventData>();
            this.Filename = Filename;
        }

        public void Add(Vector<double> Location, double ClockTime, string Species)
        {
            EventDataBag.Add(new EventData { ClockTime = ClockTime, Location = Location, Species = Species });
        }

        public void WriteContents()
        {
            TextWriter FileParticleFates
                = new StreamWriter(Environment.CurrentDirectory + "//" + Filename);

            FileParticleFates.WriteLine("Clock time\tX\tY\tZ");

            //"defining an area of 
            foreach (var EventData in EventDataBag)
            {
                FileParticleFates.WriteLine(EventData.ClockTime.ToString() + "\t"
                    + EventData.Location[0].ToString() + "\t"
                    + EventData.Location[1].ToString() + "\t"
                    + EventData.Location[2].ToString() + "\t"
                    + EventData.Species);
            }
            FileParticleFates.Flush();
            FileParticleFates.Close();
        }
    }
}

﻿using MathNet.Numerics.LinearAlgebra;
using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.IO;

namespace Aurora
{
    //Interface for profile and breakthrough events
    interface IEvent
    {
        void CheckOccurrence(Vector<double> OldLocation, Vector<double> NewLocation, double OldTime, double NewTime, Simulation Parent, Particle Particle);
        void WriteContents(String Tag, string text);
        String TextContents();
        String GetEventType();
    }


    abstract class BreakthroughEvent : IEvent
    {
        public double[] SurfaceParams;
        readonly string EventType;

        protected struct EventData
        {
            public double BreakthroughTime;
            public string Species;
        }

        protected ConcurrentBag<EventData> BreakthroughData;

        public BreakthroughEvent(double[] SurfaceParams)
        {
            this.SurfaceParams = SurfaceParams;

            BreakthroughData = new ConcurrentBag<EventData>();
            EventType = "Breakthrough";
        }

        public abstract Boolean Condition(Vector<double> Location);

        public virtual void CheckOccurrence(Vector<double> OldLocation, Vector<double> NewLocation, double OldTime, double NewTime,
            Simulation Parent, Particle Particle)
        {
            if ((Condition(OldLocation) == false) && (Condition(NewLocation) == true))
            {
                //Can be improved by interpolating the actual time of breakthrough
                double EstimatedBreakthroughTime = 0.5 * OldTime + 0.5 * NewTime;
                if (EstimatedBreakthroughTime <= Parent.MaxClockTime)
                    BreakthroughData.Add(new EventData { BreakthroughTime = EstimatedBreakthroughTime, Species = Particle.Species });
            }
        }

        public virtual string TextContents()
        { return "Default message"; }

        //Text matches the type of curve - plane or a cylinder
        public void WriteContents(String Tag, string Text)
        {
            TextWriter FileParticleFates 
                = new StreamWriter(Path.Combine(Environment.CurrentDirectory, Tag + ".btc"));

            foreach(double Coefficient in SurfaceParams)
            {
                FileParticleFates.Write(Coefficient.ToString() + "\t");
            }
            FileParticleFates.WriteLine(Text);

            //"defining an area of 
            foreach (EventData DataPoint in BreakthroughData)
            {
                FileParticleFates.WriteLine(DataPoint.BreakthroughTime.ToString() + "\t" + DataPoint.Species);
            }
            FileParticleFates.Flush();
            FileParticleFates.Close();
        }

        public String GetEventType()
        {
            return EventType;
        }
    }

    class WellBreakthroughEvent: BreakthroughEvent
    {
        private PositionData CellLocation; //Home cell of the well
        private double CellVolume; //X-Y cross-sectional area
        private Vector<double> CellCentre;

        public WellBreakthroughEvent(double[] SurfaceParams, Simulation Parent) : base(SurfaceParams) 
        {
            double XMid = SurfaceParams[0];
            double YMid = SurfaceParams[1];
            double ZMid = SurfaceParams[2] + SurfaceParams[3]/2;

            CellCentre = Vector<double>.Build.Dense(new double[3] { XMid, YMid, ZMid });
            CellLocation = Parent.GetCellIndexes(CellCentre);
            CellVolume = (Parent.XBoundArray[CellLocation.XCellIndex + 1] - Parent.XBoundArray[CellLocation.XCellIndex])
                * (Parent.YBoundArray[CellLocation.YCellIndex + 1] - Parent.YBoundArray[CellLocation.YCellIndex])
                * (Parent.ZBoundArray[CellLocation.XCellIndex, CellLocation.YCellIndex, CellLocation.ZCellIndex + 1] 
                - Parent.ZBoundArray[CellLocation.XCellIndex, CellLocation.YCellIndex, CellLocation.ZCellIndex]);

            Debug.WriteLine(CellCentre.ToString());
            Debug.WriteLine("X Min " + Parent.XBoundArray[CellLocation.XCellIndex].ToString() + " X Max " + Parent.XBoundArray[CellLocation.XCellIndex + 1].ToString());
        }

        public override bool Condition(Vector<double> Location)
        {
            throw new NotImplementedException();
        }

        bool Condition(Vector<double> Location, double Radius)
        {
            var Distance = (Location - CellCentre).L2Norm();
            return Distance <= Radius;            
        }

        public override void CheckOccurrence(Vector<double> OldLocation, Vector<double> NewLocation, double OldTime, double NewTime, Simulation Parent, Particle Particle)
        {
            double Radius;
            int TimeStepIdx = Parent.GetTimeStepIndex((OldTime + NewTime) / 2);
            double XPlusFlux = Parent.GetXPlusFlux(CellLocation, TimeStepIdx);
            double XMinusFlux = Parent.GetXMinusFlux(CellLocation, TimeStepIdx);
            double YPlusFlux = Parent.GetYPlusFlux(CellLocation, TimeStepIdx);
            double YMinusFlux = Parent.GetYMinusFlux(CellLocation, TimeStepIdx);
            int FAIdx = Parent.GetFluxArrayIndexFromTSIndex(TimeStepIdx);
            double ZPlusFlux = Parent.ZFluxArray[CellLocation.XCellIndex, CellLocation.YCellIndex, CellLocation.ZCellIndex + 1, FAIdx];
            double ZMinusFlux = Parent.ZFluxArray[CellLocation.XCellIndex, CellLocation.YCellIndex, CellLocation.ZCellIndex, FAIdx];

            double NetFlux = XMinusFlux - XPlusFlux + YMinusFlux - YPlusFlux + ZMinusFlux - ZPlusFlux;
            double InFlux = Math.Max(XMinusFlux,0) + Math.Max(-XPlusFlux, 0) + Math.Max(YMinusFlux, 0) + Math.Max(-YPlusFlux, 0);
            double OutFlux = Math.Max(-XMinusFlux, 0) + Math.Max(XPlusFlux, 0) + Math.Max(-YMinusFlux, 0) + Math.Max(YPlusFlux, 0);
            double WellOutFlux = Math.Max(InFlux - OutFlux, 0); //If more in than out, positive flow up well.

            //Capture correct fraction of particles through cell with hydraulic radius.
            try
            {
                Radius = Math.Pow((CellVolume * WellOutFlux / InFlux)/Math.PI*3/4, 0.333333);
            }
            catch (DivideByZeroException)  //Flow is only outward
            {
                Radius = 0;
            }

            if (Condition(NewLocation, Radius) == true)
            {
                double EstimatedBreakthroughTime = 0.5 * OldTime + 0.5 * NewTime;
                if (EstimatedBreakthroughTime <= Parent.MaxClockTime)
                    BreakthroughData.Add(new EventData { BreakthroughTime = EstimatedBreakthroughTime, Species = Particle.Species });
                
                Particle.TerminationStatus = TerminationStatus.OutOfBounds;  //Particle is sucked up the well
            }
        }
    }

    class TubeBreakthroughInEvent: BreakthroughEvent
    {
        public TubeBreakthroughInEvent(double[] SurfaceParams) : base(SurfaceParams) { }
        
        public override string TextContents()
        {
            string Text = "\t#Six coefficients, x0 y0 rx ry zmin height, defining a breakthrough elliptic cylinder with bottom centre at x0,y0,zmin";
            return Text;
        }

        public override Boolean Condition(Vector<double> Location)
        {
            bool Inside = false;
            double XMid = SurfaceParams[0]; 
            double YMid = SurfaceParams[1];
            double ZMin = SurfaceParams[2];
            double Radius = SurfaceParams[3];
            double Height = SurfaceParams[4];

            var Metric = Math.Pow((Location[0] - XMid) / Radius, 2) + Math.Pow((Location[1] - YMid) / Radius, 2);
            if (Location[2] > ZMin && Location[2] < ZMin + Height && Metric <= 1)
            {
                Inside = true;
            }
            return Inside;
        }

    }

    class TubeBreakthroughOutEvent : TubeBreakthroughInEvent
    {
        public TubeBreakthroughOutEvent(double[] SurfaceParams) : base(SurfaceParams) { }

        public override Boolean Condition(Vector<double> Location)
        {
            return !base.Condition(Location);
        }
    }

    class TubeBreakthroughEitherEvent : TubeBreakthroughInEvent
    {
        public TubeBreakthroughEitherEvent(double[] SurfaceParams) : base(SurfaceParams) { }

        public override void CheckOccurrence(Vector<double> OldLocation, Vector<double> NewLocation, double OldTime, double NewTime, 
            Simulation Parent, Particle Particle)
        {
            if (Condition(OldLocation) != Condition(NewLocation))
            {
                //Can be improved by interpolating the actual time of breakthrough
                double EstimatedBreakthroughTime = 0.5 * OldTime + 0.5 * NewTime;
                BreakthroughData.Add(new EventData { BreakthroughTime = EstimatedBreakthroughTime, Species = Particle.Species });
            }
        }
    }

    class PlaneBreakthroughInEvent : BreakthroughEvent
    {
        public PlaneBreakthroughInEvent(double[] SurfaceParams) : base(SurfaceParams) { }

        public override string TextContents()
        {
            string Text = "\t#Four coefficients, a b c d, defining breakthrough plane aX + bY + cZ = d)";
            return Text;
        }

        public override Boolean Condition(Vector<double> Location)
        {
            bool Inside = false;
            double a = SurfaceParams[0];
            double b = SurfaceParams[1];
            double c = SurfaceParams[2];
            double d = SurfaceParams[3];
            if (a * Location[0] + b * Location[1] + c * Location[2] - d <= 0)
            {
                Inside = true;
            }
            return Inside;
        }

        public override void CheckOccurrence(Vector<double> OldLocation, Vector<double> NewLocation, double OldTime, double NewTime, 
            Simulation Parent, Particle Particle)
        {
            if (Condition(OldLocation) != Condition(NewLocation))
            {
                //Can be improved by interpolating the actual time of breakthrough
                double EstimatedBreakthroughTime = 0.5 * OldTime + 0.5 * NewTime;
                if (EstimatedBreakthroughTime <= Parent.MaxClockTime)
                    BreakthroughData.Add(new EventData { BreakthroughTime = EstimatedBreakthroughTime, Species = Particle.Species });
            }
        }
    }

    class PlaneBreakthroughOutEvent : PlaneBreakthroughInEvent
    {
        public PlaneBreakthroughOutEvent(double[] SurfaceParams) : base(SurfaceParams) { }

        public override Boolean Condition(Vector<double> Location)
        {
            return !base.Condition(Location);
        }

    }

    class PlaneBreakthroughEitherEvent : PlaneBreakthroughInEvent
    {
        public PlaneBreakthroughEitherEvent(double[] SurfaceParams) : base(SurfaceParams) { }


        public override void CheckOccurrence(Vector<double> OldLocation, Vector<double> NewLocation, double OldTime, double NewTime, 
            Simulation Parent, Particle Particle)
        {
            if (Condition(OldLocation) != Condition(NewLocation))
            {
                //Can be improved by interpolating the actual time of breakthrough
                double EstimatedBreakthroughTime = 0.5 * OldTime + 0.5 * NewTime;
                if (EstimatedBreakthroughTime <= Parent.MaxClockTime)
                    BreakthroughData.Add(new EventData { BreakthroughTime = EstimatedBreakthroughTime, Species = Particle.Species });
            }
        }
    }

    class ProfileEvent : IEvent
    {
        readonly Dictionary<string, ConcurrentBag<Vector<double>>> ProfileLocations;
        readonly double TriggerTime;
        readonly string EventType;

        public ProfileEvent(double TriggerTime, Simulation Parent)
        {
            this.TriggerTime = TriggerTime;
            
            ProfileLocations = new Dictionary<string, ConcurrentBag<Vector<double>>>();
            foreach(string Species in Parent.SpeciesList)
            {
                if (Species != "Null")
                {
                    ProfileLocations[Species] = new ConcurrentBag<Vector<double>>();
                }
            }
            EventType = "Profile";
        }

        public void CheckOccurrence(Vector<double> OldLocation, Vector<double> NewLocation, double OldTime, double NewTime,
            Simulation Parent, Particle Particle)
        {
            if (OldTime <= TriggerTime && NewTime > TriggerTime && Particle.Species != "Null")
            {
                //If a particle is still in a source, it may represnet a local MolesPerParticle
                if (Particle.AssociatedSource is not null &&
                    Parent.UniformRandom.NextDouble() > Particle.AssociatedSource.MolesPerParticle / Parent.MolesPerParticle)
                    return;
                double Interpolator = (TriggerTime - OldTime) / (NewTime - OldTime);
                ProfileLocations[Particle.Species].Add(Interpolator * NewLocation + (1 - Interpolator) * OldLocation);
            }
        }

        public string TextContents()
        { return ""; }

        public void WriteContents(String Tag, string Text)
        {
            TextWriter FileParticleFates
                = new StreamWriter(Path.Combine(Environment.CurrentDirectory, Tag + ".pro"));

            FileParticleFates.WriteLine(TriggerTime.ToString() + "\t#Time at which particles are frozen");

            foreach (string Species in ProfileLocations.Keys)
            {
                foreach (Vector<double> Location in ProfileLocations[Species])
                {
                    FileParticleFates.WriteLine(Location[0].ToString() + "\t" + Location[1].ToString()
                        + "\t" + Location[2].ToString() + "\t" + (Species == "Default" ? "" : Species));
                }
            }
            FileParticleFates.Flush();
            FileParticleFates.Close();
        }

        public String GetEventType()
        {
            return EventType;
        }
    }
}

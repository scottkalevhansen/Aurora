﻿using MathNet.Numerics.LinearAlgebra;

namespace Aurora
{
    interface ILocationSampler
    {
        Vector<double> Sample();
    }
}

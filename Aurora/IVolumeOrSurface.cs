﻿using MathNet.Numerics.LinearAlgebra;

namespace Aurora
{
    interface IVolumeOrSurface
    {
        double GetVolume();    //Interior volume or volume enclosed by surface or contained

        bool IsInside(Vector<double> Location);

        Vector<double> OutwardNormal(Vector<double> Location);

        Vector<double> RandomLocation();
    }
}

﻿using MathNet.Numerics.Distributions;
using MathNet.Numerics.RootFinding;
using System;

namespace Aurora    
{
    static class InverseGaussian
    {
        public static double Sample(double Mu, double Lambda)
        {
            double TargetValue = ContinuousUniform.Sample(0, 1);
            return Bisection.FindRootExpand(CDFGenerator(Mu, Lambda, TargetValue), 0, Mu + 5 * Math.Sqrt(Lambda));
        }

        static Func<double, double> CDFGenerator(double Mu, double Lambda, double TargetValue)
        {
            return x =>
            {
                // Formula for CDF of Inverse Gaussian is expressable in terms of the CDF for the standard Normal
                return Normal.CDF(0, 1, Math.Sqrt(Lambda / x) * (x / Mu - 1))
                    + Math.Exp(2 * Lambda / Mu) * Normal.CDF(0, 1, -Math.Sqrt(Lambda / x) * (x / Mu + 1))
                    - TargetValue;
            };
        }
    }
}

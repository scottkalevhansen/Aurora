﻿using MathNet.Numerics.Distributions;
using MathNet.Numerics.LinearAlgebra;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Aurora
{

    enum TerminationStatus
	{
        Active,
        OutOfBounds,
        OutOfTime,
        Stagnated,
        SourceDespawn,
        Decayed,
        Pumped
	}

    class Particle
    {
        public Vector<double> Location;
        public double ClockTime;
        public TerminationStatus TerminationStatus;
        public Vector<double>? Velocity;
        public string Species;
        public MolarConcentrationSource? AssociatedSource;

        public Simulation Parent;

        public Particle(Vector<double> Location, double StartTime, Simulation Parent, string Species = "Default", MolarConcentrationSource? AssociatedSource = null)
        {
            this.Location = Location;
            this.Parent = Parent;
            ClockTime = StartTime;
            TerminationStatus = TerminationStatus.Active;
            this.Species = Species;
            this.AssociatedSource = AssociatedSource;
        }

        public void Step()
        {
            if (ClockTime > Parent.MaxClockTime)
            {
                TerminationStatus = TerminationStatus.OutOfTime;
                return;
            }
            int TimeStepIndex = Parent.GetTimeStepIndex(ClockTime);
            //Console.WriteLine("Location enters Step(): [{0},{1},{2}]", Location[0], Location[1], Location[2]);
            //Locate particle in grid and move it back into the domain if transverse dispersion ha has moved it out of the domain
            //Adjust if outside the domain in XY
            //for (int i = 0; i < Parent.YBoundArray.Length; i++)
            //    Console.WriteLine("YBoundArray[i] = {0}", Parent.YBoundArray[i]);
            var Excess = Parent.XYBoundsChecker(Location);
            Location -= 2 * Excess;

            //Console.WriteLine("Location just before GetCellIndex: [{0},{1},{2}]", Location[0], Location[1], Location[2]);
            //Find cell information
            var CellLocation = Parent.GetCellIndexes(Location, TimeStepIndex);
            //Console.WriteLine("Location just after GetCellIndex: [{0},{1},{2}]", CellLocation.XCellIndex, CellLocation.YCellIndex, CellLocation.ZCellIndex);

            //Adjust if above or below the domain
            while (CellLocation.Excess is not null)
			{
                Location -= 2 * CellLocation.Excess;
                //Movement through top / bottom of domain moves particle back outside the domain in x'-y', retire particle
                if(Parent.XYBoundsChecker(Location).L2Norm() > 0)
                {
                    TerminationStatus = TerminationStatus.OutOfBounds;
                    return;
                }
                CellLocation = Parent.GetCellIndexes(Location, TimeStepIndex);
            }
                
            Velocity = Parent.GetVelocity(CellLocation, ClockTime, out TimeStepIndex);

            if (Velocity.L2Norm() == 0)
            {
                TerminationStatus = TerminationStatus.Stagnated;
                Parent.StagnationEventCollector.Add(Location, ClockTime, Species);
                return;
            }
            if (this.Species == "Null")
            {
                TerminationStatus = TerminationStatus.Decayed;
                return;
            }
            var Speed = Velocity.L2Norm();

            double OperationalTimeDelta = Parent.StreamlineDelta / Speed;

            double ClockTimeDelta = Parent.OperationalTimeToClockTime(OperationalTimeDelta, this);

            double StepReductionFactor = 1.0;
            //If the step takes you longer than the end of the current stress period...
            if (ClockTime + ClockTimeDelta >= Parent.TimeStepBoundArray[TimeStepIndex+1])
            {
                //..and if NOT beyond the end of last stress period....
                if (ClockTime < Parent.TimeStepBoundArray[TimeStepIndex + 1])
                {
                    //...fast forward to end of current stress period.
                    StepReductionFactor = (Parent.TimeStepBoundArray[TimeStepIndex + 1] - ClockTime) / ClockTimeDelta;
                    ClockTimeDelta = Parent.TimeStepBoundArray[TimeStepIndex + 1] - ClockTime;
                }
            }

            var TravelLength = Parent.StreamlineDelta;
            Vector<double> AdvectionDelta = Velocity * TravelLength * StepReductionFactor / Velocity.L2Norm();

            //Compute transverse dispersion
            //Need to find two Vectors orthonormal to Velocity, multiply them by the noise sample.
            Vector<double> OrthoH = CrossProduct(Velocity, Vector<double>.Build.Dense(new double[] { 0, 0, 1 }));
            if (OrthoH.L2Norm() < 1e3 * Double.Epsilon)
            {
                //Flow is essentially vertical; assign x as the "horizontal" dispersion direction
                OrthoH = Vector<double>.Build.Dense(new double[] { 1, 0, 0 });
            }
            else
            {
                OrthoH /= OrthoH.L2Norm();
            }

            Vector<double> OrthoV = CrossProduct(Velocity, OrthoH);
            if (OrthoV.L2Norm() < 1e3 * Double.Epsilon)
            {
                //Flow is essentially vertical; assign x as the "horizontal" dispersion direction
                OrthoH = Vector<double>.Build.Dense(new double[] { 0, 1, 0 });
            }
            else
            {
                OrthoV /= OrthoV.L2Norm();
            }

            var LayerIndex = CellLocation.ZCellIndex;
            Vector<double> TransverseDispersionDelta = Vector<double>.Build.Dense(3, 0);
#pragma warning disable CS8602
            try { TransverseDispersionDelta += OrthoH * Math.Sqrt(StepReductionFactor) * Parent.DisperserArrayH[LayerIndex].Sample(); } catch { }
            try { TransverseDispersionDelta += OrthoV * Math.Sqrt(StepReductionFactor) * Parent.DisperserArrayV[LayerIndex].Sample(); } catch { }
#pragma warning restore CS8602

            //Check if the particle is pumped out by wells.
            /*
            double StepMove = (AdvectionDelta + TransverseDispersionDelta).L2Norm();
            Vector<double> UnitVector = (AdvectionDelta + TransverseDispersionDelta) / StepMove;
            Vector<double> IntersectingPoint;
            double[] TemporaryX;
            double[] TemporaryY;
            double[] TemporaryZ;
            double Fraction = 1;
            double CaptureProbability = 0;

            
            foreach (Simulation.WellData Well in Parent.WellFluxes)
            {
                TemporaryX = new double[] { Parent.XBoundArray[Convert.ToInt32(Well.CellIndexes[0])], Parent.XBoundArray[1 + Convert.ToInt32(Well.CellIndexes[0])] };
                TemporaryY = new double[] { Parent.YBoundArray[Convert.ToInt32(Well.CellIndexes[1])], Parent.XBoundArray[1 + Convert.ToInt32(Well.CellIndexes[1])] };
                if (Parent.HeadArray[Convert.ToInt32(Well.CellIndexes[0]), Convert.ToInt32(Well.CellIndexes[1]), Convert.ToInt32(Well.CellIndexes[2]), TimeStepIndex] 
                    > Parent.ZBoundArray[Convert.ToInt32(Well.CellIndexes[0]), Convert.ToInt32(Well.CellIndexes[1]), Convert.ToInt32(Well.CellIndexes[2])] &&
                    Parent.HeadArray[Convert.ToInt32(Well.CellIndexes[0]), Convert.ToInt32(Well.CellIndexes[1]), Convert.ToInt32(Well.CellIndexes[2]), TimeStepIndex] 
                    < Parent.ZBoundArray[Convert.ToInt32(Well.CellIndexes[0]), Convert.ToInt32(Well.CellIndexes[1]), 1 + Convert.ToInt32(Well.CellIndexes[2])])
                    TemporaryZ = new double[] { Parent.ZBoundArray[Convert.ToInt32(Well.CellIndexes[0]), Convert.ToInt32(Well.CellIndexes[1]), 
                        Convert.ToInt32(Well.CellIndexes[2])], Parent.HeadArray[Convert.ToInt32(Well.CellIndexes[0]), Convert.ToInt32(Well.CellIndexes[1]), 
                        Convert.ToInt32(Well.CellIndexes[2]), TimeStepIndex] };
                else
                    TemporaryZ = new double[] { Parent.ZBoundArray[Convert.ToInt32(Well.CellIndexes[0]), Convert.ToInt32(Well.CellIndexes[1]), Convert.ToInt32(Well.CellIndexes[2])], Parent.ZBoundArray[Convert.ToInt32(Well.CellIndexes[0]), Convert.ToInt32(Well.CellIndexes[1]), 1 + Convert.ToInt32(Well.CellIndexes[2])] };
                
                List<Vector<double>> IntersectingPoints = new List<Vector<double>> ();

                // Check if the particle within a step passed trough the cell
                foreach (double X in TemporaryX)
                {
                    if ((X - Location[0]) * UnitVector[0] > 0)
                    {
                        IntersectingPoint = Location + (X - Location[0]) / UnitVector[0] * UnitVector;
                        if (IntersectingPoint[1] >= TemporaryY[0] && IntersectingPoint[1] <= TemporaryY[1] && IntersectingPoint[2] >= TemporaryZ[0] && IntersectingPoint[2] <= TemporaryZ[1])
                            IntersectingPoints.Add(IntersectingPoint);
                    }
                }
                foreach (double Y in TemporaryY)
                {
                    if ((Y - Location[1]) * UnitVector[1] > 0)
                    {
                        IntersectingPoint = Location + (Y - Location[1]) / UnitVector[1] * UnitVector;
                        if (IntersectingPoint[0] >= TemporaryX[0] && IntersectingPoint[0] <= TemporaryX[1] && IntersectingPoint[2] >= TemporaryZ[0] && IntersectingPoint[2] <= TemporaryZ[1])
                            IntersectingPoints.Add(IntersectingPoint);
                    }
                }
                foreach (double Z in TemporaryX)
                {
                    if ((Z - Location[2]) * UnitVector[2] > 0)
                    {
                        IntersectingPoint = Location + (Z - Location[2]) / UnitVector[2] * UnitVector;
                        if (IntersectingPoint[0] >= TemporaryX[0] && IntersectingPoint[0] <= TemporaryX[1] && IntersectingPoint[1] >= TemporaryY[0] && IntersectingPoint[1] <= TemporaryY[1])
                            IntersectingPoints.Add(IntersectingPoint);
                    }
                }
                if (IntersectingPoints.Count == 1)
                {
                    if (Location[0] >= TemporaryX[0] && Location[0] <= TemporaryX[1] && Location[1] >= TemporaryY[0] && Location[1] <= TemporaryY[1] && Location[2] >= TemporaryZ[0] && Location[2] <= TemporaryZ[1])
                        Fraction = (IntersectingPoints[0] - Location).L2Norm() / StepMove;
                    else
                        Fraction = (Location - IntersectingPoints[0]).L2Norm() / StepMove;
                    CaptureProbability += Fraction * ClockTimeDelta * -1 * Well.FlowRates[TimeStepIndex] / ((TemporaryX[1] - TemporaryX[0]) * (TemporaryY[1] - TemporaryY[0]) * (TemporaryZ[1] - TemporaryZ[0]) * Parent.PorosityArray[CellLocation.ZCellIndex]);
                    if (Fraction > 1) // In case that the particle remains in the cell
                        Fraction = 1;
                }
                if (IntersectingPoints.Count == 2)
                {
                    Fraction = (IntersectingPoints[0] - IntersectingPoints[1]).L2Norm() / StepMove;
                    CaptureProbability += Fraction * ClockTimeDelta * -1 * Well.FlowRates[TimeStepIndex] / ((TemporaryX[1] - TemporaryX[0]) * (TemporaryY[1] - TemporaryY[0]) * (TemporaryZ[1] - TemporaryZ[0]) * Parent.PorosityArray[CellLocation.ZCellIndex]);
                }
            }
            if (Parent.UniformRandom.NextDouble() < CaptureProbability)
            {
                TerminationStatus = TerminationStatus.Pumped;
            }
            */

            foreach (IEvent Event in Parent.OutputEvents)
            {
                Event.CheckOccurrence(Location, Location + AdvectionDelta + TransverseDispersionDelta, 
                    ClockTime, ClockTime + ClockTimeDelta, Parent, this);

                if (TerminationStatus != TerminationStatus.Active) return; //If well or other feature absorbs the particle
            }

            //Check if decay to another species occurs during the specified timestep.
            double MinimumDecayDelta = Parent.MaxClockTime;
            int NumDestinations = 0;
            string[] DecayDestinations = new string[1]; //Useless initialization to please compiler
            int[] DecayDestinationMoles = new int[1];
            foreach (DecayRule Rule in Parent.DecayDictionary[Species])
                {
                    double RuleDecayTime = Exponential.Sample(Rule.Rate);
                    if (RuleDecayTime < MinimumDecayDelta)
                    {
                        MinimumDecayDelta = RuleDecayTime;
                        DecayDestinations = Rule.Destinations;
                        DecayDestinationMoles = Rule.DestinationMoles;
                        NumDestinations = DecayDestinations.Length;
                    }
                }

            if (AssociatedSource is not null) //only perform calculation is particle is still associated with some source
            {
                foreach (SourceEvent Event in Parent.MolarSourceEvents)
                {
                    var NewLocation = Location + AdvectionDelta + TransverseDispersionDelta;
                    var (Status, TransitionTime) = Event.CheckTransition(Location, NewLocation,
                        ClockTime, ClockTime + ClockTimeDelta, MinimumDecayDelta, this.Species);

                    if (Status == TransitionStatus.EpochChange || Status == TransitionStatus.Enter)
                    {
                        TerminationStatus = TerminationStatus.SourceDespawn; //Kill particle when it enters 
                        return;
                    }
                    else if (Status == TransitionStatus.Exit)
                    {
                        Event.Source.ReplaceParticle(TransitionTime); //Replace outbound particle
                        if (Parent.UniformRandom.NextDouble() < AssociatedSource!.MolesPerParticle / Parent.MolesPerParticle)
                        {
                            AssociatedSource = null;
                            //If particle leaves source AND domain due to dispersion, ensure it is retired and not placed
                            //back into the domain on the next time step by the bounds checker at the start of next step.
                            if (Parent.XYBoundsChecker(NewLocation).L2Norm() > 0)
                            {
                                TerminationStatus = TerminationStatus.OutOfBounds;
                                return;
                            }
                            if (Parent.GetCellIndexes(NewLocation, Parent.GetTimeStepIndex(ClockTime + ClockTimeDelta)).Excess is not null)
                            {
                                TerminationStatus = TerminationStatus.OutOfBounds;
                                return;
                            }
                        }
                        else
                        {
                            TerminationStatus = TerminationStatus.SourceDespawn;
                            return;
                        }
                    }
                    else if (Status == TransitionStatus.Decayed)
                    {
                        //Particle will be eliminated, but to keep the source concentration constant, we must replace it
                        Event.Source.ReplaceParticle(TransitionTime);
                        TerminationStatus = TerminationStatus.Decayed;
                        return;
                    }
                }
            }

            ClockTime += ClockTimeDelta;
            Location += AdvectionDelta;

            if (Parent.XYBoundsChecker(Location).L2Norm() > 0) //If advection takes it OOB, retire the particle
            {
                TerminationStatus = TerminationStatus.OutOfBounds;
                Parent.OutOfBoundsEventCollector.Add(Location, ClockTime, Species);
                return;
            }
            Location += TransverseDispersionDelta;

            //If particle active, but should decay: Implment decay by creating a new particle and eliminating the present particle
            if (MinimumDecayDelta <= ClockTimeDelta && MinimumDecayDelta < Parent.MaxClockTime)
            {
                TerminationStatus = TerminationStatus.Decayed;
                for (int ChildIdx = 0; ChildIdx < NumDestinations; ChildIdx++ )
				{
                    for(int ParticleIdx = 0; ParticleIdx < DecayDestinationMoles[ChildIdx]; ParticleIdx++)
					{
                        Particle NextParticle = new(Location, ClockTime, Program.Simulation, DecayDestinations[ChildIdx]);
                        Program.Simulation.ParticleQueue.Enqueue(NextParticle);
                    }
                }
            }
        }

        public static Vector<double> CrossProduct(Vector<double> V1, Vector<double> V2)
        {
            if (V1.Count != 3 || V2.Count != 3)
                throw new Exception("Vectors must be of length 3");

            Vector<double> Result = new MathNet.Numerics.LinearAlgebra.Double.DenseVector(3)
            {
                [0] = V1[1] * V2[2] - V1[2] * V2[1],
                [1] = -V1[0] * V2[2] + V1[2] * V2[0],
                [2] = V1[0] * V2[1] - V1[1] * V2[0]
            };

            return Result;
        }
            
    }
}

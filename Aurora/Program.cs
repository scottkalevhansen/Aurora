﻿using System;

namespace Aurora
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>

        public static Simulation Simulation;

        static void Main(String[] CommandLineArguments) //Get Args
        {
            if (CommandLineArguments.Length < 1 || CommandLineArguments.Length > 2 )
            {
                Console.WriteLine("Too many (or too few) command line arguments");
                Environment.Exit(-1);
            }
            String MarshalFolder = CommandLineArguments[0];
            String MarshalFile = CommandLineArguments.Length > 1 ? CommandLineArguments[1] : "Marshal.txt";

            Simulation = new Simulation(MarshalFolder, MarshalFile);
            Simulation.Run();

            int BreakthroughTag = 1;
            int ProfileTag = 1;

            foreach (IEvent OutputEvent in Simulation.OutputEvents)
            {
                if (OutputEvent.GetEventType() == "Profile")
                {
                    OutputEvent.WriteContents(ProfileTag.ToString(), "");
                    ProfileTag++;
                }
                else
                {
                    string Text = OutputEvent.TextContents();
                    OutputEvent.WriteContents(BreakthroughTag.ToString(),Text);
                    BreakthroughTag++;
                }
            }
            Simulation.StagnationEventCollector.WriteContents();
            Simulation.OutOfBoundsEventCollector.WriteContents();
        }
    }
}

﻿using MathNet.Numerics.Distributions;
using MathNet.Numerics.LinearAlgebra;
using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.IO;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace Aurora
{
    /* A NOTE ON COORDINATE SYSTEMS:
     * 
     * Regrettably, different "coordinate systems" must be used: 
     *      <X, Y, Z> (exact particle location),
     *      <Column, Row, Layer> (cell indexes defining the discretized velocity field)
     *      <Right, Front, Up> (cell faces for fluxes, inherited from GW_Chart)
     *      
     * We align them so that particles move in traditional <X,Y,Z> coordinates, with
     *      Increasing X moving rightwards, increasing column index
     *      Increasing Y moving frontwards, increasing row index
     *      Increasing Z moving upwards,    increasing layer index
     * 
     * We refer to <Columns, Rows, Layers> and <Right, Front, Up> as little as we can. 
     * Instead:
     *      Right and left faces --> XPlus and XMinus faces
     *      Front and back faces --> YPlus and YMinus faces
     *      Column index --> XCellIndex
     *      Row index --> YCellIndex
     *      Layer index --> ZCellIndex
     */

    partial class Simulation
    {
        //Utilities
        public static readonly char[] Delimiters = {' ', '\t'};
        public const int ProgressCheckInterval = 1000;
        public int MaxThreads = Math.Min(12, Environment.ProcessorCount-1);
        const double AssimilationWeight = 0.01;
        public const StringSplitOptions REE = StringSplitOptions.RemoveEmptyEntries;
        public const int SpecialTimeStepIndex = -1;
        public const double DegreesToRadians = 2 * 3.14159268 / 360;

        //Class variables
        public string MarshalFolder;
        public string MarshalFile;

        public double[] XBoundArray;
        public double[] YBoundArray;
        public double[,,] ZBoundArray;


        /*
         * Each interlayer surface is treated as a tesselation of triangles, forming a continuous surface. 
         * Each layer joins nodes at the corners of the cells in the x'-y' plane, and layer interface n
         * represents the average of the bottom elevations of the four adjoining cells in layer n, or,
         * in the case of the model top interface, the top elevations of the top layer cells. Vectors
         * storing the locations of these nodes are stored in CellCornerArray, indexed by interface index
         * (there is one more x' interface than number of cells in the x' direction, and these increase
         * with increasing cell x' index).
         * 
         * Nodes defining the same interface are joined their neighbours as they are in the x'-y' grid in
         * MODFLOW, to form rectangles. Additional line segments are added, within each layer, from each
         * cell's smallest x', smallest y' corner to its largest x', largest y' corner, dividing each
         * rectangle into two triangles. For each cell (seen in map view), for each layer, one triangle
         * occupies the "NW" corner (larger y', smaller x'), and one the "SE" corner (smaller y', larger
         * x'). Each plane is defined by a normal vector in the +z direction. The NW coner has index 0 in
         * LayerInterfaceNormalArrays, and the SE coner has index 1.
         */
        Vector<double>[,,] CellCornerArray;
        Vector<double>[][,,] LayerInterfaceNormalArrays;
        Vector<double>[,,,]? HeadCellCornerArray;

        double[,,,] XPlusFluxArray;
        double[,,,] YMinusFluxArray;
        public double[,,,] ZFluxArray; //Inconsistent with others, the Z index represents a FACE, not CELL
        public double[,,,] HeadArray;
        public double[,,,] RechargeRate;

        public List<int> StressPeriodCumulativeSteps; //Item i: num timesteps complete by end of stree period i
        public List<int> FluxArrayIndexToStepIndex; //Iten i: timestep corresponding to FluxArray i
        public List<int> HeadArrayIndexToStepIndex; //Iten i: timestep corresponding to FluxArray i

        public double MaxClockTime;
        public double StreamlineDelta;

        public int NumCellsY; //number of cells in the x' direction (internal x coordinate)
        public int NumCellsX; //number of cells in the y' direction
        public int NumCellsZ; //number of cell in the z (vertical) direction 
        public int NumTimeSteps;
        public int NumStressPeriods;

        public double[] TimeStepBoundArray;
                
        public EventCollector StagnationEventCollector;
        public EventCollector OutOfBoundsEventCollector;
        public List<IEvent> OutputEvents;
        public List<Source> Sources;
        public List<PrecipitationSource> PrecipitationSources;
        public List<SourceEvent> MolarSourceEvents;

        public ConcurrentQueue<Particle> ParticleQueue;
        public double? MolesPerParticle;
        public double[] PorosityArray;
        public Normal?[] DisperserArrayH;
        public Normal?[] DisperserArrayV;
        public Subordinator[] AdvectiveSubordinatorArray;
        public Subordinator[] MIMTSubordinatorArray;

        public List<String> SpeciesList;
        public Dictionary<String, DecayRules> DecayDictionary;
        public Dictionary<String, double[]> MIMTScalingDictionary;

        public bool IsRotated;
        public bool AutoOffset;
        public Vector<double> GridCornerOffset;  //In global coordinates
        public double GridAngle; 
        public Matrix<double>? GridToGlobalRotationMatrix;
        public Matrix<double>? GlobalToGridRotationMatrix;
        public Random UniformRandom;

        public HeadSurfaceExcessDelegate HeadSurfaceExcess;

        Dictionary<TerminationStatus, int> FateDictionary;
        private readonly object DictionaryLock = new();
        double ActiveCompletionEstimate;

        public struct WellData
        {
            public Vector<double> CellIndexes;
            public double[] FlowRates;
        }
        public List<WellData> WellFluxes;

        //Code so that clicking in the console does not halt output to the console
        [DllImport("kernel32.dll", SetLastError = true)]
        static extern IntPtr GetStdHandle(int nStdHandle);

        [DllImport("kernel32.dll")]
        static extern bool GetConsoleMode(IntPtr hConsoleHandle, out uint lpMode);

        [DllImport("kernel32.dll")]
        static extern bool SetConsoleMode(IntPtr hConsoleHandle, uint dwMode);


#pragma warning disable CS8618
        public Simulation(String MarshalFolder, String MarshalFile)
        {
            this.MarshalFolder = MarshalFolder;
            this.MarshalFile = MarshalFile;
            
            SetConsoleClick(false); //User can't mistakenly halt operation by clicking in console
            
            try{Directory.SetCurrentDirectory(MarshalFolder);}catch{ Console.WriteLine("Couldn't switch to marshal directory"); Environment.Exit(-1);}
            Console.WriteLine("Parsing discretization information and cell water budgets beginning at " + DateTime.Now.ToString("hh:mm:ss"));
            Console.WriteLine("This could take some time...\n");
            try {  InititializeVariablesAndClasses(); }catch{ Console.WriteLine("Error parsing input files"); Environment.Exit(-2);}

            FateDictionary = new Dictionary<TerminationStatus, int>()
            {
                [TerminationStatus.Decayed] = 0,
                [TerminationStatus.OutOfBounds] = 0,
                [TerminationStatus.OutOfTime] = 0,
                [TerminationStatus.SourceDespawn] = 0,
                [TerminationStatus.Stagnated] = 0,
                [TerminationStatus.Pumped] = 0
            };
            ActiveCompletionEstimate = 0;
            GC.Collect();
        }
#pragma warning restore CS8618

        static bool SetConsoleClick(bool ClickOn)
        {
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                const uint ENABLE_QUICK_EDIT = 0x0040;
                const int STD_INPUT_HANDLE = -10;  // (DWORD): -10 is the standard input device.
                IntPtr ConsoleHandle = GetStdHandle(STD_INPUT_HANDLE);

                // get current console mode
                uint ConsoleMode;
                if (!GetConsoleMode(ConsoleHandle, out ConsoleMode))
                    return false; // ERROR: Unable to get console mode.

                if (!ClickOn)
                    ConsoleMode &= ~ENABLE_QUICK_EDIT;
                else
                    ConsoleMode |= ENABLE_QUICK_EDIT;

                // set the new mode
                if (!SetConsoleMode(ConsoleHandle, ConsoleMode))
                    return false; // ERROR: Unable to set console mode

                return true;
            }
            else { return false; }
        }

        public async Task DispatchParticleProcessorsAsync()
        {
            var ParticleProcessorTasks = new List<Task>();
            for (int n = 0; n < MaxThreads; n++)
            {
                ParticleProcessorTasks.Add(Task.Run(() => {
                    Particle? CurrentParticle;
                    //var WorkerID = new Random().Next(0, int.MaxValue);
                    while (ParticleQueue.TryDequeue(out CurrentParticle))
                    {
                        while (CurrentParticle.TerminationStatus == TerminationStatus.Active)
                        {
                            CurrentParticle.Step();

                            ActiveCompletionEstimate = AssimilationWeight * (Math.Min(CurrentParticle.ClockTime / MaxClockTime, 1))
                                + (1 - AssimilationWeight) * ActiveCompletionEstimate;
                        }
                        lock (DictionaryLock)
                        {
                            FateDictionary[CurrentParticle.TerminationStatus]++;
                        }
                    }
                }));
            }
            await Task.WhenAll(ParticleProcessorTasks);
        }

        public void Run()
        {

            foreach (var Source in Sources)
            {
                Source.InitializeParticlePositions();
            }
            foreach (var PrecipitationSource in PrecipitationSources)
            {
                PrecipitationSource.InitializeParticlePositions();
            }
            Console.WriteLine("Simulation beginning with " + ParticleQueue.Count.ToString() + " particles at " + DateTime.Now.ToString("hh:mm:ss"));

            Task ParticleProcessing = DispatchParticleProcessorsAsync();
            Console.Write("Estimated completion: ");
            //ParticleProcessing.Wait();
            
            using (var ProgressMeter = new ProgressBar())
            {
                ProgressMeter.Report(0.0);
                
                while (!ParticleProcessing.IsCompleted) //Update progress bar periodically
                {
                    Task.Delay(200).Wait();
                    
                    double FractionAlive = (double)ParticleQueue.Count
                        / (ParticleQueue.Count + FateDictionary[TerminationStatus.OutOfTime]
                        + FateDictionary[TerminationStatus.OutOfBounds] + FateDictionary[TerminationStatus.Stagnated] + FateDictionary[TerminationStatus.Pumped]);
                    var CompletionEstimate = ActiveCompletionEstimate;// * FractionAlive + (1 - FractionAlive);
                    ProgressMeter.Report(CompletionEstimate);
                    
                }
                
                ProgressMeter.Report(1.0);
            }
            Debug.WriteLine("Leftover particles " + ParticleQueue.Count.ToString());
            
            Console.WriteLine("100%\nSimulation completed at " + DateTime.Now.ToString("hh:mm:ss"));
            Console.WriteLine("");
            Console.WriteLine("Number decayed: " + FateDictionary[TerminationStatus.Decayed].ToString());
            Console.WriteLine("Number out of bounds: " + FateDictionary[TerminationStatus.OutOfBounds].ToString());
            Console.WriteLine("Number out of time: " + FateDictionary[TerminationStatus.OutOfTime].ToString());
            Console.WriteLine("Number despawned: " + FateDictionary[TerminationStatus.SourceDespawn].ToString());
            Console.WriteLine("Number stagnated: " + FateDictionary[TerminationStatus.Stagnated].ToString());
            Console.WriteLine("Number pumped: " + FateDictionary[TerminationStatus.Pumped].ToString());


            SetConsoleClick(true); //User can copy results
        }

        public int GetTimeStepIndex(double ClockTime)
        {
            int TimeStepIndex;
            //Use the last stress period for the remainder of the simulation
            if (ClockTime >= TimeStepBoundArray[^1])
            {
                TimeStepIndex = TimeStepBoundArray.Length - 2;
            }
            else
            {
                //for (TimeStepIndex = 0; ClockTime < TimeStepBoundArray[TimeStepIndex+1]; TimeStepIndex++);
                TimeStepIndex = 1;
                while (ClockTime >= TimeStepBoundArray[TimeStepIndex])
                {
                    TimeStepIndex++;
                }
                TimeStepIndex--;
            }
            return TimeStepIndex;
        }

        public int GetArrayIndexFromTimeStepIndex(int TSIdx, List<int> ArrayTSIndexes)
        {
            int AIdx = -1;
            foreach (int ATSIdx in ArrayTSIndexes)
            {
                if (ATSIdx > TSIdx) break;
                AIdx++;
            }
            return Math.Max(0,AIdx); //If first array is after first timestep, still use that one
        }

        public int GetFluxArrayIndexFromTSIndex(int TSIdx)
        { return GetArrayIndexFromTimeStepIndex(TSIdx, FluxArrayIndexToStepIndex); }

        public int GetHeadArrayIndexFromTSIndex(int TSIdx)
        { return GetArrayIndexFromTimeStepIndex(TSIdx, HeadArrayIndexToStepIndex); }


        public PositionData GetCellIndexes(Vector<double> Location)
        {
            var CellLocation = new PositionData
            {
                XCellIndex = 0, // These indices represent the CELLS, not the boundaries
                YCellIndex = 0,
                ZCellIndex = NumCellsZ //Represents OOB
            };
            //Console.WriteLine("GetCellIndex enter with [{0},{1},{2}]", Location[0], Location[1], Location[2]);
            Vector<double> GridLocation = GlobalToGridRotationMatrix*(Location - GridCornerOffset);

            while (XBoundArray[CellLocation.XCellIndex + 1] < GridLocation[0])
            {
                CellLocation.XCellIndex++; 
            }
            CellLocation.XFraction = (GridLocation[0] - XBoundArray[CellLocation.XCellIndex]) /
                (XBoundArray[CellLocation.XCellIndex + 1] - XBoundArray[CellLocation.XCellIndex]);
            

            while (YBoundArray[CellLocation.YCellIndex + 1] < GridLocation[1])
            {
                CellLocation.YCellIndex++; 
            }
            CellLocation.YFraction = (GridLocation[1] - YBoundArray[CellLocation.YCellIndex]) / 
                (YBoundArray[CellLocation.YCellIndex + 1] - YBoundArray[CellLocation.YCellIndex]);
            //Console.WriteLine("    Cell XYIndex = [{0},{1}]", CellLocation.XCellIndex, CellLocation.YCellIndex);

            double Delta = 0, LastDelta = 1; //always should be negative; positive indicates no previous evaluations
            Vector<double> Corner, Normal;

            int TriangleSelectIdx = CellLocation.YFraction > CellLocation.XFraction ? 0 : 1; // 0 if in NW corner, 1 if SE

            //Only set if particle is not OOB high or low
            for (int InterfaceIndex = 0; InterfaceIndex <= NumCellsZ; InterfaceIndex++)
            {
                //If NW, TriangleSelectIdx = 0, corner at <CellLocation.XCellIndex,     CellLocation.YCellIndex + 1>
                //If SE, TriangleSelectIdx = 1, corner at <CellLocation.XCellIndex + 1, CellLocation.YCellIndex>
                Corner = CellCornerArray[CellLocation.XCellIndex + TriangleSelectIdx, CellLocation.YCellIndex + 1 - TriangleSelectIdx, InterfaceIndex];
                Normal = LayerInterfaceNormalArrays[TriangleSelectIdx][CellLocation.XCellIndex, CellLocation.YCellIndex, InterfaceIndex];

                Delta = (GridLocation - Corner).DotProduct(Normal)/Normal.L2Norm();
                /*
                 * A plane passing through <x0,y0,z0>, satisfies 0 = a(x-x0) + b(y-y0) - (z-z0). A parallel plane through <x0,y0,z'>
                 * satisfies (z0-z') = a(x-x0) + b(y-y0) - (z-z0). Evaluating the RHS will be positive if <x,y,z> is below the
                 * plane, and negative if above. We start from the bottom of the domain and keep evaluating for the planes defining
                 * interfaces until we find a plane ABOVE the particle. The particle's layer index is then set to one less than this
                 * interface index. If no interace is found (particle is OOB high), the ZCellIndex is never changed from its intial
                 * value of NumCellsZ (which is one more than the highest layer index). If the particle is below the bottom interface
                 * (OOB low), ZCellIndex is set to -1.
                 * 
                 * ZFraction is only set if the particle is inside the domain. Otherwise, it is null. This is set to the ratio of the
                 * normal distance from the lower plane to the total of the normal distances from the particle to each of the two
                 * planes bounding it. This approximates the quantity we want: the fraction of the angle between the planes of a
                 * notional new plane sharing their line of intersection and passing through the particle.
                 */

                //Delta is distance from plane to particle in direction of DOWNWARD-pointing normal vector
                if (Delta > 0) //If we found a plane above the particle
				{
                    if (LastDelta < 0) //Only if not on first iteration (i.e., not OOB low)
                    {
                        CellLocation.ZFraction = Math.Abs(LastDelta) / (Math.Abs(LastDelta) + Math.Abs(Delta));
                    }
                    CellLocation.ZCellIndex = InterfaceIndex - 1; //Will be different
                    break;
                }
                LastDelta = Delta;
			}

            //If OOB in the Z direction, we have to generate the excess vector
            if (CellLocation.ZCellIndex == NumCellsZ)
			{
                Normal = LayerInterfaceNormalArrays[TriangleSelectIdx][CellLocation.XCellIndex, CellLocation.YCellIndex, NumCellsZ];
                CellLocation.Excess = GridToGlobalRotationMatrix * (Delta * Normal / Normal.L2Norm());
            }
            else if (CellLocation.ZCellIndex == -1)
            {
                var SlopeVector = LayerInterfaceNormalArrays[TriangleSelectIdx][CellLocation.XCellIndex, CellLocation.YCellIndex, 0];
                CellLocation.Excess = GridToGlobalRotationMatrix * (Delta * SlopeVector / SlopeVector.L2Norm());
            }
            return CellLocation;
        }

        public PositionData GetCellIndexes(Vector<double> Location, int TimeStepIdx)
        {
            var CellLocation = GetCellIndexes(Location);
            if (CellLocation.Excess is null)
            {
                Vector<double> GridLocation = GlobalToGridRotationMatrix * (Location - GridCornerOffset);
                int TriangleSelectIdx = CellLocation.YFraction > CellLocation.XFraction ? 0 : 1; // 0 if in NW corner, 1 if SE
                HeadSurfaceExcess(Location, ref CellLocation, GridLocation, TriangleSelectIdx, TimeStepIdx);
            }
            return CellLocation;
        }

        public delegate void HeadSurfaceExcessDelegate(Vector<double> Location, ref PositionData CellLocation,
            Vector<double> GridLocation, int TriangleSelectIdx, int TimeStepIdx);

        public void AssumeConfinedSurfaceExcess(Vector<double> Location, ref PositionData CellLocation,
            Vector<double> GridLocation, int TriangleSelectIdx, int TimeStepIdx)
        {
        }

        public void VariableHeadSurfaceExcess(Vector<double> Location, ref PositionData CellLocation,
            Vector<double> GridLocation, int TriangleSelectIdx, int TimeStepIdx)
        {
            var HeadCorner = HeadCellCornerArray[CellLocation.XCellIndex + TriangleSelectIdx, 
                CellLocation.YCellIndex + 1 - TriangleSelectIdx, CellLocation.ZCellIndex, TimeStepIdx].SubVector(0, 3);
            var HeadNormal = Particle.CrossProduct((HeadCellCornerArray[CellLocation.XCellIndex + 1,
                CellLocation.YCellIndex + 1, CellLocation.ZCellIndex, TimeStepIdx]).SubVector(0, 3) - HeadCorner,
            (HeadCellCornerArray[CellLocation.XCellIndex, CellLocation.YCellIndex, CellLocation.ZCellIndex, TimeStepIdx]).SubVector(0, 3) - HeadCorner);
            HeadNormal /= -1.0 * HeadNormal[2];
            var HeadDelta = (GridLocation - HeadCorner).DotProduct(HeadNormal) / HeadNormal.L2Norm();

            if (HeadDelta < 0) //The particle is above the head surface
                CellLocation.Excess = GridToGlobalRotationMatrix * (HeadDelta * HeadNormal / HeadNormal.L2Norm());
        }


        public double OperationalTimeToClockTime(double OperationalTime, Particle Particle)
        {
            var CellLocation = GetCellIndexes(Particle.Location);

            double MovementTime = AdvectiveSubordinatorArray[CellLocation.ZCellIndex].SubordinationMap(OperationalTime, Particle);

            double ClockTime = MIMTSubordinatorArray[CellLocation.ZCellIndex].SubordinationMap(MovementTime, Particle);

            return ClockTime;
        }

        public Vector<double> GetVelocity(Vector<double> Location, double ClockTime, out int TimeStepIndex)
		{
            var CellLocation = GetCellIndexes(Location);

            return GetVelocity(CellLocation, ClockTime, out TimeStepIndex);
        }

        delegate double GetFluxDelegate(PositionData CellLocation, int TimeStepIdx);

        double GetFaceVelocityFromFlux(GetFluxDelegate FluxFunction, PositionData CellLocation, int TimeStepIndex, double Area, double Porosity)
        {
            if (Area > 0)
                return FluxFunction(CellLocation, TimeStepIndex) / Area / Porosity;
            else
                return 0;
        }

        double ComputeSideAreaWrapper(PositionData CellLocation, CellFace Cell, int TimeStepIdx)
        {
            int[] Corner1XY, Corner2XY;
            double Width;

            switch (Cell)
            {
                case CellFace.MinusX:
                    Corner1XY = new int[] { CellLocation.XCellIndex, CellLocation.YCellIndex };
                    Corner2XY = new int[] { CellLocation.XCellIndex, CellLocation.YCellIndex + 1 };
                    Width = YBoundArray[CellLocation.YCellIndex + 1] - YBoundArray[CellLocation.YCellIndex];
                    break;
                
                case CellFace.PlusX:
                    Corner1XY = new int[] { CellLocation.XCellIndex + 1, CellLocation.YCellIndex };
                    Corner2XY = new int[] { CellLocation.XCellIndex + 1, CellLocation.YCellIndex + 1 };
                    Width = YBoundArray[CellLocation.YCellIndex + 1] - YBoundArray[CellLocation.YCellIndex];
                    break;

                case CellFace.MinusY:
                    Corner1XY = new int[] { CellLocation.XCellIndex, CellLocation.YCellIndex };
                    Corner2XY = new int[] { CellLocation.XCellIndex + 1, CellLocation.YCellIndex };
                    Width = XBoundArray[CellLocation.XCellIndex + 1] - XBoundArray[CellLocation.XCellIndex];
                    break;

                default:  //case CellFace.PlusY; keeps the compiler happy
                    Corner1XY = new int[] { CellLocation.XCellIndex, CellLocation.YCellIndex + 1 };
                    Corner2XY = new int[] { CellLocation.XCellIndex + 1, CellLocation.YCellIndex + 1 };
                    Width = XBoundArray[CellLocation.XCellIndex + 1] - XBoundArray[CellLocation.XCellIndex];
                    break;
            }
            return ComputeSideArea(Corner1XY, Corner2XY, CellLocation.ZCellIndex, Width, TimeStepIdx);
        }

        double ComputeSideArea(int[] CellCorner1XYIdx, int[] CellCorner2XYIdx, int CellBottomXIdx, double Width, int TimeStepIdx)
        {
            var HAIdx = GetHeadArrayIndexFromTSIndex(TimeStepIdx);

            double CellSideArea = Width * (CellCornerArray[CellCorner1XYIdx[0], CellCorner1XYIdx[1], CellBottomXIdx + 1][2]
                    - CellCornerArray[CellCorner1XYIdx[0], CellCorner1XYIdx[1], CellBottomXIdx][2]
                    + CellCornerArray[CellCorner2XYIdx[0], CellCorner2XYIdx[1], CellBottomXIdx + 1][2]
                    - CellCornerArray[CellCorner2XYIdx[0], CellCorner2XYIdx[1], CellBottomXIdx][2]) / 2;
            if (HeadCellCornerArray is null) //ASSUME_SATURATED is active, so we just use cell shape
            {
                return CellSideArea;
            }
            else
            {
                // Cell bound elevation MINUS water table elevation in cell (POSITIVE IF UNSATURATED)
                var DeltaTop1 = CellCornerArray[CellCorner1XYIdx[0], CellCorner1XYIdx[1], CellBottomXIdx + 1][2]
                    - HeadCellCornerArray[CellCorner1XYIdx[0], CellCorner1XYIdx[1], CellBottomXIdx, HAIdx][2];
                var DeltaTop2 = CellCornerArray[CellCorner2XYIdx[0], CellCorner2XYIdx[1], CellBottomXIdx + 1][2]
                    - HeadCellCornerArray[CellCorner2XYIdx[0], CellCorner2XYIdx[1], CellBottomXIdx, HAIdx][2];
                var DeltaBottom1 = CellCornerArray[CellCorner1XYIdx[0], CellCorner1XYIdx[1], CellBottomXIdx][2]
                    - HeadCellCornerArray[CellCorner1XYIdx[0], CellCorner1XYIdx[1], CellBottomXIdx, HAIdx][2];
                var DeltaBottom2 = CellCornerArray[CellCorner2XYIdx[0], CellCorner2XYIdx[1], CellBottomXIdx][2]
                    - HeadCellCornerArray[CellCorner2XYIdx[0], CellCorner2XYIdx[1], CellBottomXIdx, HAIdx][2];

                if (DeltaTop1 <= 0 && DeltaTop2 <= 0) // Fully saturated cell, water table ABOVE TOP everywhere
                {
                    return CellSideArea;
                }
                else if (DeltaBottom1 >= 0 && DeltaBottom2 >= 0) // Fully unsaturated cell, water table BELOW BOTTOM everywhere
                {
                    return 0; // value is irrelevant; should never apply to any particle
                }
                else if(DeltaTop1 * DeltaTop2 < 0) // Partially unsaturated cell; water table ABOVE TOP at only one corner
                {
                    if (DeltaBottom1 * DeltaBottom2 < 0) // Water table BELOW BOTTOM in one corner, ABOVE TOP in other
                    {
                        var RB = Math.Max(DeltaBottom1, DeltaBottom2) / (Math.Abs(DeltaBottom1) + Math.Abs(DeltaBottom2));
                        var RT = Math.Max(DeltaTop1, DeltaTop2) / (Math.Abs(DeltaTop1) + Math.Abs(DeltaTop2));
                        // Height should be the distance between top and bottom lines at ration RT across the cell 
                        var Height = CellSideArea / Width; // Approximate as average value; fix later
                        return 0.5 * (RT - RB) * Width * Height + (1 - RT) * CellSideArea;
                    }
                    else // Water table IN CELL in one corner, ABOVE TOP in other
                    {
                        double SaruratedSideArea 
                            = Width * (HeadCellCornerArray[CellCorner1XYIdx[0], CellCorner1XYIdx[1], CellBottomXIdx, HAIdx][2]
                            - CellCornerArray[CellCorner1XYIdx[0], CellCorner1XYIdx[1], CellBottomXIdx][2]
                            + HeadCellCornerArray[CellCorner2XYIdx[0], CellCorner2XYIdx[1], CellBottomXIdx, HAIdx][2]
                            - CellCornerArray[CellCorner2XYIdx[0], CellCorner2XYIdx[1], CellBottomXIdx][2]) / 2;
                        var RT = Math.Max(DeltaTop1, DeltaTop2) / (Math.Abs(DeltaTop1) + Math.Abs(DeltaTop2));
                        return RT * SaruratedSideArea + (1 - RT) * CellSideArea;
                    }
                }
                else // Partially unsaturated cell; water table IN CELL in one corner, BELOW BOTTOM in other
                {
                    var RB = Math.Max(DeltaBottom1, DeltaBottom2) / (Math.Abs(DeltaBottom1) + Math.Abs(DeltaBottom2));
                    return 0.5 * (1 - RB) * Width * Math.Abs(Math.Min(DeltaBottom1, DeltaBottom2));
                }
            }
        }

        public Vector<double> GetVelocity(PositionData CellLocation, double ClockTime, out int TimeStepIndex)
        {
            // In terms of the fluxes in the RIGHT-FRONT-LOWER face system used in the budget files.
            // INCREASING X' INCREASES XCellIndex, MOVES "RIGHT" TO LARGER  CBC COLUMN INDEXES
            // INCREASING Y' INCREASES YCellIndex, MOVES "BACK"  TO SMALLER CBC ROW INDEXES  
            // INCREASING Z  INCREASES ZCellIndex, MOVES "UPPER" TO SMALLER CBC LAYER INDEXES

            // Locating the current particle in the right column, row and layer
            // These indices represent the CELLS, not the boundaries; X and Y refer to internal coordinates x' and y' 
            var XCIdx = CellLocation.XCellIndex; 
            var YCIdx = CellLocation.YCellIndex;
            var ZCIdx = CellLocation.ZCellIndex;

            //setting the stress period according to ClockTime
            TimeStepIndex = GetTimeStepIndex(ClockTime);
            var TSIdx = TimeStepIndex;

            if (CellLocation.ZFraction is null)
                throw new Exception("Attempted to compute velocity of out of bounds particle.");
            var FractionZ = (double) CellLocation.ZFraction!;

            double Porosity = PorosityArray[ZCIdx];

            double AreaMinusX = ComputeSideAreaWrapper(CellLocation, CellFace.MinusX, TSIdx);
            double AreaPlusX = ComputeSideAreaWrapper(CellLocation, CellFace.PlusX, TSIdx);
            double AreaMinusY = ComputeSideAreaWrapper(CellLocation, CellFace.MinusY, TSIdx);
            double AreaPlusY = ComputeSideAreaWrapper(CellLocation, CellFace.PlusY, TSIdx);

            double AreaZ = (XBoundArray[XCIdx + 1] - XBoundArray[XCIdx]) * (YBoundArray[YCIdx + 1] - YBoundArray[YCIdx]);

            //Compute velocities from flux array
            double VelocityX = GetFaceVelocityFromFlux(GetXPlusFlux, CellLocation, TimeStepIndex, AreaPlusX, Porosity) * CellLocation.XFraction
                + GetFaceVelocityFromFlux(GetXMinusFlux, CellLocation, TimeStepIndex, AreaMinusX, Porosity) * (1 - CellLocation.XFraction);
            double VelocityY = GetFaceVelocityFromFlux(GetYPlusFlux, CellLocation, TimeStepIndex, AreaPlusY, Porosity) * CellLocation.YFraction
                + GetFaceVelocityFromFlux(GetYMinusFlux, CellLocation, TimeStepIndex, AreaMinusY, Porosity) * (1 - CellLocation.YFraction);

            int FAIdx = GetFluxArrayIndexFromTSIndex(TSIdx);
            double VelocityZ = (ZFluxArray[XCIdx, YCIdx, ZCIdx + 1, FAIdx] * FractionZ
                + ZFluxArray[XCIdx, YCIdx, ZCIdx, FAIdx] * (1 - FractionZ)) / AreaZ / Porosity;

            //Add additional vertical fluxes to account for layer height irregularity.
            int TriangleSelectIdx = CellLocation.YFraction > CellLocation.XFraction ? 0 : 1; // 0 if in NW corner, 1 if SE
            var LocalNormal = LayerInterfaceNormalArrays[TriangleSelectIdx][XCIdx,YCIdx,ZCIdx+1]*FractionZ 
                + LayerInterfaceNormalArrays[TriangleSelectIdx][XCIdx, YCIdx, ZCIdx] * (1-FractionZ);
            
            double DeltaZByDeltaX = LocalNormal[0];
            VelocityZ += VelocityX * DeltaZByDeltaX;
            
            double DeltaZByDeltaY = LocalNormal[1];
            VelocityZ += VelocityY * DeltaZByDeltaY;

            var GridVelocity = Vector<double>.Build.Dense(new double[3] { VelocityX, VelocityY, VelocityZ });
            return GridToGlobalRotationMatrix * GridVelocity;
        }

        public Vector<double> XYBoundsChecker(Vector<double> Location)
        {
            double ExcessX = 0;
            double ExcessY = 0;

            Vector<double> GridLocation = GlobalToGridRotationMatrix * (Location - GridCornerOffset);

            if (GridLocation[0] < XBoundArray[0])
            {
                ExcessX = GridLocation[0] - XBoundArray[0];
            }
            else if (GridLocation[0] > XBoundArray[^1])
            {
                ExcessX = GridLocation[0] - XBoundArray[^1];
            }

            if (GridLocation[1] < YBoundArray[0])
            {
                ExcessY = GridLocation[1] - YBoundArray[0];
            }
            else if (GridLocation[1] > YBoundArray[^1])
            {
                ExcessY = GridLocation[1] - YBoundArray[^1];
            }

            var Excess = Vector<double>.Build.Dense(new double[3] { ExcessX, ExcessY, 0 });
            return GridToGlobalRotationMatrix * Excess;
        }

        public double GetRechargeRate(Vector<double> CurrentLocation, double EpochStartTime)
        {
            int XIndex = GetXIndex((int) CurrentLocation[0]);
            int YIndex = GetYIndex((int) CurrentLocation[1]);
            double Head = GetHead(XIndex, YIndex, EpochStartTime);
            int ZIndex = GetLayerIndex(XIndex, YIndex, Head);
            double Recharge = RechargeRate[XIndex, YIndex, ZIndex, (int) CurrentLocation[3]];
            return Recharge;
        }

        public int GetXIndex(double GridXLocation)
        {
            int XCellIndex = 0;
            while (XBoundArray[XCellIndex + 1] < GridXLocation)
            {
                XCellIndex++;
            }
            return XCellIndex;
        }
        public int GetYIndex(double GridYLocation)
        {
            int YCellIndex = 0;
            while (YBoundArray[YCellIndex + 1] < GridYLocation)
            {
                YCellIndex++;
            }
            return YCellIndex;
        }
        public int GetLayerIndex(Vector<double> Location)
        {
            var CellLocation = GetCellIndexes(Location);
            return GetLayerIndex(CellLocation.XCellIndex, CellLocation.YCellIndex, Location[2]);
        }
        public int GetLayerIndex(int XCellIdx, int YCellIdx, double ZDirectionLocation)
        {
            for (int i = 0; i < ZBoundArray.GetLength(2); i++)
            {
                if (ZBoundArray[XCellIdx, YCellIdx, i] <= ZDirectionLocation)
                {
                    try
                    {
                        if (ZBoundArray[XCellIdx, YCellIdx, i + 1] > ZDirectionLocation)
                            return i;
                    }
                    catch (IndexOutOfRangeException)
                    {
                        return 0;
                    }
                }
                else
                    Console.WriteLine("The particle is out of the domain bound in z direction (Error in GetLayerIndex)");
            }
            // Invalid case (the particle is out of bound)
            return -1;

        }
        public double GetHead(int XCellIdx, int YCellIdx, double EpochStartTime)
        {
            /* Default: Dry cells in all layers. Locate the water table surface based on the particle's x-y Location.
             * If more than one aquifer, returns the highest surface.
             */
            double Head = ZBoundArray[XCellIdx, YCellIdx, 0]; 
                                                              
            int TimeStepIndex = 0;

            for (int i = 0; i < TimeStepBoundArray.Length; i++)
            {
                if (TimeStepBoundArray[i] >= EpochStartTime)
                {
                    TimeStepIndex = i;
                    break;
                }
            }

            for (int i = 0; i < HeadArray.GetLength(2); i++)
            {
                if (HeadArray[XCellIdx, YCellIdx, i, TimeStepIndex] <= ZBoundArray[XCellIdx, YCellIdx, i + 1])
                {
                    if (HeadArray[XCellIdx, YCellIdx, i, TimeStepIndex] > ZBoundArray[XCellIdx, YCellIdx, i])
                    {
                        Head = HeadArray[XCellIdx, YCellIdx, i, TimeStepIndex];
                    }
                }
                else
                {
                    Head = ZBoundArray[XCellIdx, YCellIdx, i + 1];
                }
            }

            return Head;
        }
    }
}
﻿using MathNet.Numerics.LinearAlgebra;
using System;
using System.IO;

namespace Aurora
{
	partial class Simulation
	{
        //Parse the geometry file and populate the arrays containing the limits of each cell
        void SetBoundArrays(String GeometryFileName)
        {
            string[] Input = File.ReadAllLines(GeometryFileName);
            char[] Delimiters = { ' ', '\t' };
            char[] ModelMuseCommentDelimiters = { ' ', '\t', '(', ',', ')' };
            int RemainingFields;
            string[] Nuggets;
            int i = 0;

            try
            {
                double XOffset = 0, YOffset = 0, GridAngle=0;
                do
                {
                    Nuggets = Input[i++].Trim().Split(ModelMuseCommentDelimiters, REE);
                    if (Nuggets[1].ToUpper() == "LOWER" && Nuggets[2].ToUpper() == "LEFT")
                    {
                        XOffset = Convert.ToDouble(Nuggets[4]);
                        YOffset = Convert.ToDouble(Nuggets[5]);
                    }
                    else if (Nuggets[1].ToUpper() == "GRID" && Nuggets[2].ToUpper() == "ANGLE")
					{
                        GridAngle = Convert.ToDouble(Nuggets[^1]);
                    }
                } while (Nuggets[0] == "#");

                if (AutoOffset == true)
				{
                    GridCornerOffset = Vector<double>.Build.Dense(new double[] { XOffset, YOffset, 0 });
                    this.GridAngle = GridAngle;

                    GenRotationMatrices();
                }

                NumCellsZ = Convert.ToInt16(Nuggets[0]);
                NumCellsY = Convert.ToInt16(Nuggets[1]);
                NumCellsX = Convert.ToInt16(Nuggets[2]);
                NumStressPeriods = Convert.ToInt16(Nuggets[3]);
            } catch { Console.WriteLine("Error reading model dimensions / periods from .dis file"); throw; }

            XBoundArray = new double[NumCellsX + 1]; //X
            YBoundArray = new double[NumCellsY + 1]; //Y
            ZBoundArray = new double[NumCellsX, NumCellsY, NumCellsZ + 1]; //Z  - contains the thickness of each cell

            i += 1;

            //X-direction: load column discretization data
            XBoundArray[0] = 0;
			try { 
                Nuggets = Input[i++].Split(Delimiters, REE);
                if (Nuggets[0] == "CONSTANT") //In case the column width is a constant value
                {
                    double XDelta = Convert.ToDouble(Nuggets[1]);
                    for (int t = 1; t <= NumCellsX; t++)
                    {
                        XBoundArray[t] = XBoundArray[t - 1] + XDelta;
                    }
                }
                else //Explicit widths for each column
                {
                    RemainingFields = NumCellsX;
                    do
                    {
                        Nuggets = Input[i++].Split(Delimiters, REE);
                        foreach (string Entry in Nuggets)
                        {
                            RemainingFields -= 1;
                            double XDelta = Convert.ToDouble(Entry);
                            XBoundArray[NumCellsX - RemainingFields]
                            = XBoundArray[NumCellsX - RemainingFields - 1] + XDelta;
                        }
                    } while (RemainingFields > 0);
                }
            }
            catch { Console.WriteLine("Error reading X discretization from .dis file"); throw; }

            //Y-direction: load row discretization data
            YBoundArray[NumCellsY] = 0; //STARTING AT THE TOP FOR Y, WILL BE CORRECTED
            try
            {
                Nuggets = Input[i++].Split(Delimiters, REE);
                if (Nuggets[0] == "CONSTANT") //In case the row height is a constant value
                {
                    double YDelta = Convert.ToDouble(Nuggets[1]);
                    for (int t = NumCellsY - 1; t >= 0; t--)
                    {
                        YBoundArray[t] = YBoundArray[t + 1] - YDelta; //SUBTRACTING from max Y value
                    }
                }
                else //Explicit widths for each row
                {
                    RemainingFields = NumCellsY;
                    do
                    {
                        Nuggets = Input[i++].Split(Delimiters, REE);
                        foreach (string Entry in Nuggets)
                        {
                            RemainingFields -= 1;
                            var YDelta = Convert.ToDouble(Entry);
                            YBoundArray[RemainingFields]
                            = YBoundArray[RemainingFields + 1] - YDelta;
                        }
                    } while (RemainingFields > 0);
                }
                //BOTTOM SHOULD HAVE COORDINATE ZERO; ADD + OFFSET TO ALL ENTRIES
                var GridYMax = -YBoundArray[0];
                for(int c = 0; c <= NumCellsY; c++)
                {
                    YBoundArray[c] += GridYMax;
                }
            } catch { Console.WriteLine("Error reading Y discretization from .dis file"); throw; }
            //Load layer discretization data
            try
            {
                for (int ZCellBufferIndex = 0; ZCellBufferIndex <= NumCellsZ; ZCellBufferIndex++)
                {
                    Nuggets = Input[i++].Split(Delimiters, REE);
                    if (Nuggets[0] == "CONSTANT")
                    {
                        for (int XCellIndex = 0; XCellIndex < NumCellsX; XCellIndex++)
                        {
                            for (int YCellIndex = 0; YCellIndex < NumCellsY; YCellIndex++)
                            {
                                //We must FLIP in the Z direction
                                int ZCellIndex = NumCellsZ - ZCellBufferIndex;
                                ZBoundArray[XCellIndex, YCellIndex, ZCellIndex] = Convert.ToDouble(Nuggets[1]);
                            }
                        }
                    }
                    else
                    {
                        double[] Buffer = new double[NumCellsX * NumCellsY];

                        int NumNuggetsRead = 0;
                        while (NumNuggetsRead < NumCellsY * NumCellsX)
                        {
                            Nuggets = Input[i++].Split(Delimiters, REE);

                            for (int k = 0; k < Nuggets.Length; k++)
                            {
                                Buffer[NumNuggetsRead + k] = Convert.ToDouble(Nuggets[k]);
                            }
                            NumNuggetsRead += Nuggets.Length;
                        }

                        for (int XCellIndex = 0; XCellIndex < NumCellsX; XCellIndex++)
                        {
                            for (int YCellBufferIndex = 0; YCellBufferIndex < NumCellsY; YCellBufferIndex++)
                            {
                                //Again, we must FLIP in the Y and Z directions because lowest indexing 
                                int YCellIndex = NumCellsY - YCellBufferIndex - 1;
                                int ZCellIndex = NumCellsZ - ZCellBufferIndex;
                                ZBoundArray[XCellIndex, YCellIndex, ZCellIndex]
                                    = Buffer[YCellBufferIndex * NumCellsX + XCellIndex];
                            }
                        }
                    }

                }
            } catch { Console.WriteLine("Error reading layer information from .dis file"); throw; }
            try
            {
                //Find the number of timesteps
                for (int LineIdx = i; LineIdx < Input.Length; LineIdx++)
                {
                    Nuggets = Input[LineIdx].Split(Delimiters, REE);
                    NumTimeSteps += Convert.ToInt16(Nuggets[1]);
                }

                TimeStepBoundArray = new double[NumTimeSteps + 1];//In fact time starts at zero - array can be shorter by one
                TimeStepBoundArray[0] = 0;

                int TimeStepIdx = 1;
                StressPeriodCumulativeSteps = new();
                for (int StressPeriodIdx = 0; StressPeriodIdx < NumStressPeriods; StressPeriodIdx++)
                {
                    Nuggets = Input[StressPeriodIdx + i].Split(Delimiters, REE);
                    
                    double StressPeriodLength = Convert.ToDouble(Nuggets[0]);
                    int NumStepsInPeriod = Convert.ToInt16(Nuggets[1]);
                    if (StressPeriodIdx == 0)
                        StressPeriodCumulativeSteps.Add(NumStepsInPeriod);
                    else
                        StressPeriodCumulativeSteps.Add(StressPeriodCumulativeSteps[StressPeriodIdx - 1] + NumStepsInPeriod);
                    
                    double TimeStepMultiplier = Convert.ToDouble(Nuggets[2]);
                    double CurrentStepLength;

                    if (TimeStepMultiplier > 1.0001)
                    {
                        CurrentStepLength = StressPeriodLength * (TimeStepMultiplier - 1) / (Math.Pow(TimeStepMultiplier, NumStepsInPeriod) - 1);
                    }
                    else
                    {
                        CurrentStepLength = StressPeriodLength / NumStepsInPeriod;
                    }

                    for (int CurrentStepInPeriodIdx = 0; CurrentStepInPeriodIdx < NumStepsInPeriod; CurrentStepInPeriodIdx++)
                    {
                        TimeStepBoundArray[TimeStepIdx] = TimeStepBoundArray[TimeStepIdx - 1] + CurrentStepLength;
                        CurrentStepLength *= TimeStepMultiplier;
                        TimeStepIdx++;
                    }
                }
            } catch { Console.WriteLine("Error reading timesteps from .dis file"); throw; }

            //Generate spectial arrays used for localization of particles to cells
            SetCellCornerArray();
            SetLayerInterfaceSlopeArray();
        }

        void SetCellCornerArray() 
        {
            CellCornerArray = new Vector<double>[NumCellsX + 1, NumCellsY + 1, NumCellsZ + 1];

            for (int XIdx = 0; XIdx <= NumCellsX; XIdx++)
			{
                for (int YIdx = 0; YIdx <= NumCellsY; YIdx++)
				{
                    for (int ZIdx = 0; ZIdx <= NumCellsZ; ZIdx++)
					{
                        double VertexZTYally = 0;
                        int NumCellsAveraged = 0;

                        //Average of neighbouring cells that are not out of bounds
                        for (int i = XIdx - 1; i <= XIdx; i++)
                            for (int j = YIdx - 1; j <= YIdx; j++)
                                if (i >= 0 && i < NumCellsX && j >= 0 && j < NumCellsY)
								{
                                    VertexZTYally += ZBoundArray[i, j, ZIdx];
                                    NumCellsAveraged++;
                                }

                        CellCornerArray[XIdx,YIdx,ZIdx] = Vector<double>.Build.Dense(
                            new double[3] { XBoundArray[XIdx], YBoundArray[YIdx], VertexZTYally / NumCellsAveraged});
					}
				}
			}
        }

        void SetLayerInterfaceSlopeArray()
        {
            Vector<double> NewVector;
            LayerInterfaceNormalArrays = new[] { new Vector<double>[NumCellsX, NumCellsY, NumCellsZ + 1], new Vector<double>[NumCellsX, NumCellsY, NumCellsZ + 1] };

            for (int XCellIdx = 0; XCellIdx < NumCellsX; XCellIdx++)
            {
                for (int YCellIdx = 0; YCellIdx < NumCellsY; YCellIdx++)
                {
                    for (int ZIdx = 0; ZIdx <= NumCellsZ; ZIdx++)
                    {
                        //Compute normal vector for NW corner; unit magnitude in -z direction
                        NewVector = Particle.CrossProduct(CellCornerArray[XCellIdx + 1, YCellIdx+1, ZIdx] - CellCornerArray[XCellIdx, YCellIdx+1, ZIdx],
                            CellCornerArray[XCellIdx, YCellIdx, ZIdx] - CellCornerArray[XCellIdx, YCellIdx+1, ZIdx]);
                        NewVector /= -1.0*NewVector[2];
                        LayerInterfaceNormalArrays[0][XCellIdx, YCellIdx, ZIdx] = NewVector;

                        //Compute normal vector for SE corner; unit magnitude in -z direction
                        NewVector = Particle.CrossProduct(CellCornerArray[XCellIdx + 1, YCellIdx + 1, ZIdx] - CellCornerArray[XCellIdx + 1, YCellIdx, ZIdx],
                            CellCornerArray[XCellIdx, YCellIdx, ZIdx] - CellCornerArray[XCellIdx + 1, YCellIdx, ZIdx]);
                        NewVector /= -1.0 * NewVector[2];
                        LayerInterfaceNormalArrays[1][XCellIdx, YCellIdx, ZIdx] = NewVector;
                    }
                }
            }
        }
    }
}
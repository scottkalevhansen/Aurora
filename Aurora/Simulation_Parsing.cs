﻿using MathNet.Numerics;
using MathNet.Numerics.Distributions;
using MathNet.Numerics.LinearAlgebra;
using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.IO;
using System.Threading.Tasks;

namespace Aurora
{
    /* A NOTE ON COORDINATE SYSTEMS:
     * 
     * Regrettably, different "coordinate systems" must be used: 
     *      <X, Y, Z> (exact particle location),
     *      <Column, Row, Layer> (cell indexes defining the discretized velocity field)
     *      <Right, Front, Up> (cell faces for fluxes, inherited from GW_Chart)
     *      
     * We align them so that particles move in traditional <X,Y,Z> coordinates, with
     *      Increasing X moving rightwards, increasing column index
     *      Increasing Y moving frontwards, increasing row index
     *      Increasing Z moving upwards,    increasing layer index
     * 
     * We refer to <Columns, Rows, Layers> and <Right, Front, Up> as little as we can. 
     * Instead:
     *      Right and left faces --> XPlus and XMinus faces
     *      Front and back faces --> YPlus and YMinus faces
     *      Column index --> XCellIndex
     *      Row index --> YCellIndex
     *      Layer index --> ZCellIndex
     */

    partial class Simulation
    {

        void InititializeVariablesAndClasses()
        {
            UniformRandom = new Random();
            StagnationEventCollector = new EventCollector("stagnation-events.txt");
            OutOfBoundsEventCollector = new EventCollector("OOB-events.txt");
            OutputEvents = new List<IEvent>();
            Sources = new List<Source>();
            PrecipitationSources = new List<PrecipitationSource>();
            MolarSourceEvents = new List<SourceEvent>();
            ParticleQueue = new ConcurrentQueue<Particle>();
            SpeciesList = new List<string>(new string[] {"Default", "Null"});
            DecayDictionary = new Dictionary<string, DecayRules>()
            {
                {"Default", new DecayRules() },
                {"Null", new DecayRules() }
            };
            MIMTScalingDictionary = new Dictionary<string, double[]>();

            //Read domain parameters from file and define derived quantities
            string[] Input = File.ReadAllLines(MarshalFile);
            int i = 0;
            
            var BlockTitleList = new List<string>(new string[] 
                {"MAIN", "DOMAIN", "LAYER", "SPECIES", "DECAY", "MIMT_ADJUSTMENT", "SOURCE", "MOLAR_SOURCE", "BREAKTHROUGHS",
                    "PROFILES", "PRECIPITATION_SOURCE"});
            while (i < Input.Length)
            {
                string FirstNugget;
                string[] Nuggets = Input[i++].Split(Delimiters);
                if (BlockTitleList.Contains(Nuggets[0].ToUpper()))
                {
                    int FirstLineIndex = i;
                    do
                    {
                        FirstNugget = Input[i++].Split(Delimiters)[0].ToUpper();
                    } while (FirstNugget != "END");
                    int LastLineIndex = i - 2;

                    //Console.WriteLine(Nuggets[0].ToUpper());
                    switch (Nuggets[0].ToUpper())
                    {
                        case "MAIN":
                            try{ ParseMainBlock(Input, FirstLineIndex); } 
                            catch { Console.WriteLine("Error parsing MAIN block"); throw;}
                            break;
                        case "DOMAIN":
                            try{ ParseDomainBlock(Input, FirstLineIndex); } 
                            catch { Console.WriteLine("Error parsing DOMAIN block"); throw; }
                            break;
                        case "LAYER":
                            try { 
                                int LayerIndex = Convert.ToInt16(Nuggets[1]);
                                ParseLayerBlock(Input, FirstLineIndex, LayerIndex);
                            } catch { Console.WriteLine("Error parsing LAYER block"); throw; }
                            break;
                        case "SPECIES":
                            try { ParseSpeciesBlock(Input, FirstLineIndex, LastLineIndex); } 
                            catch { Console.WriteLine("Error parsing SPECIES block"); throw; }
                            break;
                        case "DECAY":
                            try { ParseDecayBlock(Input, FirstLineIndex, LastLineIndex); }
                            catch { Console.WriteLine("Error parsing DECAY block"); throw; }
                            break;
                        case "MIMT_ADJUSTMENT":
                            try { ParseMIMTBlock(Input, FirstLineIndex, LastLineIndex); }
                            catch { Console.WriteLine("Error parsing MIMT_ADJUSTMENT block"); throw; }
                            break;
                        case "SOURCE":
                            try { ParseSourceBlock(Input, FirstLineIndex, LastLineIndex); } 
                            catch { Console.WriteLine("Error parsing SOURCE block"); throw; }
                            break;
                        case "MOLAR_SOURCE":
                            try { ParseMolarSourceBlock(Input, FirstLineIndex); }
                            catch { Console.WriteLine("Error parsing MOLAR_SOURCE block"); throw; }
                            break;
                        case "PRECIPITATION_SOURCE":
                            try { ParsePrecipitationBlock(Input, FirstLineIndex); }
                            catch { Console.WriteLine("Error parsing PRECIPITATION_SOURCE block"); throw; }
                            break;
                        case "BREAKTHROUGHS":
                            try{ ParseBreakthroughBlock(Input, FirstLineIndex, LastLineIndex); } 
                            catch { Console.WriteLine("Error parsing BREAKTHROUGHS block"); throw; }
                            break;
                        case "PROFILES":
                            try{ ParseProfileBlock(Input, FirstLineIndex, LastLineIndex); } 
                            catch { Console.WriteLine("Error parsing PROFILES block"); throw; }
                            break;

                        default:
                            throw new Exception("Unrecognized block title");
                    }
                }
            }
			EncodeDefaultMIMTBehaviour();
        }

        static double ParseToDouble(string Line)
        {
            return Convert.ToDouble(Line.Split(Delimiters, REE)[0]);
        }

        static List<String> ParseSubblock(in string[] Input, ref int CurrentLineIndex)
        {
            var Output = new List<String>();
            var ImmediateReturns = new List<String> { "NONE", "AUTO_GRID_OFFSET", "ASSUME_SATURATED" };
            var Nuggets = Input[CurrentLineIndex++].Split(Delimiters, REE);
            Output.Add(Nuggets[0].ToUpper());

            if (ImmediateReturns.Exists(s => s == Output[0])) return Output; //Special case of an empty block

            if (Nuggets.Length > 1 && Nuggets[1] == "->")   //Inline form
            {
                for (int i = 2; i < Nuggets.Length; i++)
                {
                    Output.Add(Nuggets[i]);
                }
            }
            else
            {
                while(true)
                {
                    Nuggets = Input[CurrentLineIndex++].Split(Delimiters, REE);
                    if (Nuggets[0] == "ESB") break;
                    else if (Nuggets[0][0] == '[')
					{
                        var Idx = 0;
                        do
                        {
                            Output.Add(Nuggets[Idx++]);
                        } while (Nuggets[Idx - 1][^1] != ']');
					}
                    else Output.Add(Nuggets[0]);    
                } 
            }
            return Output;
        }

        static List<List<String>> ParseSubblockWithTuples(in string[] Input, ref int CurrentLineIndex)
        {
            var RawList = ParseSubblock(Input, ref CurrentLineIndex);
            var OutList = new List<List<String>>();
            var CurrTuple = new List<string>();
            bool InTuple = false;
            foreach (string Item in RawList)
			{
                if (Item == "[") InTuple = true;
                else if (Item == "]") InTuple = false;
                else if (Item[0] == '[')
                {
                    InTuple = true;
                    CurrTuple.Add(Item[1..]);
                }
                else if (Item[^1] == ']')
                {
                    InTuple = false;
                    CurrTuple.Add(Item[..^1]);
                }
                else CurrTuple.Add(Item);

                if (!InTuple)
                {
                    OutList.Add(CurrTuple);
                    CurrTuple = new List<string>();
                }
			}
            return OutList;
        }

        void GenRotationMatrices()
		{
            IsRotated = true;
            var Theta = (double) GridAngle * DegreesToRadians;
            GridToGlobalRotationMatrix =
                Matrix<double>.Build.DenseOfArray(new double[,]
                {{Trig.Cos(Theta), -Trig.Sin(Theta), 0 }, {Trig.Sin(Theta), Trig.Cos(Theta), 0 }, {0, 0, 1}});
            GlobalToGridRotationMatrix =
                Matrix<double>.Build.DenseOfArray(new double[,]
                {{Trig.Cos(-Theta), -Trig.Sin(-Theta), 0 }, {Trig.Sin(-Theta), Trig.Cos(-Theta), 0 }, {0, 0, 1}});

        }

        void ParseMainBlock(in string[] Input, int FirstLineIndex)
        {
            int i = FirstLineIndex;
            String GeometryFileName = Path.Combine(MarshalFolder, Input[i++].Split(Delimiters, REE)[0]);

            var HeadInfo = ParseSubblock(Input, ref i); 

            var BudgetFile = ParseSubblock(Input, ref i);

            StreamlineDelta = Convert.ToDouble(Input[i++].Split(Delimiters, REE)[0]);
            MaxClockTime = Convert.ToDouble(Input[i++].Split(Delimiters, REE)[0]);

            var GridParams = ParseSubblockWithTuples(Input, ref i);

            if (Input[i].Split(Delimiters, REE)[0] != "END") //Particle moles are specified
            {
                MolesPerParticle = Convert.ToDouble(ParseSubblock(Input, ref i)[1]);
            }

            if (GridParams[0][0] == "MANUAL_GRID_OFFSET")
            {
                AutoOffset = false;

                var XOffset = Convert.ToDouble(GridParams[1][0]);
                var YOffset = Convert.ToDouble(GridParams[1][1]);
                GridCornerOffset = Vector<double>.Build.Dense(new double[] { XOffset, YOffset, 0 });
                GridAngle = Convert.ToDouble(GridParams[2][0]);

                GenRotationMatrices();
            }
            else if (GridParams[0][0] == "AUTO_GRID_OFFSET")
            {
                AutoOffset = true;
            }
            else
            {
                Console.WriteLine("Must specify AUTO_GRID_OFFSET or MANUAL_GRID_OFFSET subblock");
                throw new Exception("Missing grid offset option (new in v. 1.4)");
            }

            SetBoundArrays(GeometryFileName); // Set domains

            switch (HeadInfo[0])
            {
                case "ASSUME_SATURATED":
                    HeadSurfaceExcess = AssumeConfinedSurfaceExcess;
                    break;

                case "BHD_FILE":
                    HeadSurfaceExcess = VariableHeadSurfaceExcess;

                    String HeadOutputFileName = Path.Combine(MarshalFolder, HeadInfo[1]);
                    try { GenerateHeadArray(HeadOutputFileName, out HeadArray); }
                    catch { Console.WriteLine("Error reading head arrays"); throw; }
                    break;

                default:
                    throw new Exception("Must specify 'ASSUME_SATURATED' or 'BHD_FILE'");

            }

            switch (BudgetFile[0])
            {
                case "CBC_FILE":
                    String CellByCellFileName = Path.Combine(MarshalFolder,BudgetFile[1]);
                    try
                    {
                        CellByCellFluxArray(CellByCellFileName);
                    }
                    catch { Console.WriteLine("Error computing flux arrays"); throw; }

                    break;

                case "GW_CHART_FILES":
                    String RightFluxFileName = Path.Combine(MarshalFolder, BudgetFile[1]);
                    String FrontFluxFileName = Path.Combine(MarshalFolder, BudgetFile[2]);
                    String LowerFluxFileName = Path.Combine(MarshalFolder, BudgetFile[3]);
                    try
                    {
                        Task ProcessXBudget = Task.Run(() =>
                        {
                            if (NumCellsX > 1)
                            {
                                SetXPlusFluxArray(GenerateRawFluxArray(RightFluxFileName), out XPlusFluxArray);
                            }
                            else
                            {
                                SetBlankXOrYFluxArray(out XPlusFluxArray);
                            }
                        });

                        Task ProcessYBudget = Task.Run(() =>
                        {
                            if (NumCellsY > 1)
                            {
                                SetYMinusFluxArray(GenerateRawFluxArray(FrontFluxFileName), out YMinusFluxArray);
                            }
                            else
                            {
                                SetBlankXOrYFluxArray(out YMinusFluxArray);
                            }
                        });

                        Task ProcessZBudget = Task.Run(() =>
                        {
                            if (NumCellsZ > 1)
                            {
                                SetZFluxArray(GenerateRawFluxArray(LowerFluxFileName), out ZFluxArray);
                            }
                            else
                            {
                                SetBlankZFluxArray(out ZFluxArray);  //Fill with zeros
                            }
                        });

                        Task.WaitAll(ProcessXBudget, ProcessYBudget, ProcessZBudget);
                    }
                    catch { Console.WriteLine("Error computing flux arrays"); throw; }

                    break;

                default:
                    throw new Exception("Must specify 'CBC_FILE' or 'GW_CHART_FILES'");
            }

            PorosityArray = new double[NumCellsZ];
            DisperserArrayH = new Normal[NumCellsZ];
            DisperserArrayV = new Normal[NumCellsZ];
            AdvectiveSubordinatorArray = new Subordinator[NumCellsZ];
            MIMTSubordinatorArray = new Subordinator[NumCellsZ];
        }

        void ParseLayerBlock(in string[] Input, int FirstLineIndex, int LayerIndex)
        {
            int i = FirstLineIndex;
            var Nuggets = Input[i].Split(Delimiters, REE);
            PorosityArray[LayerIndex] = Convert.ToDouble(Nuggets[0]); i++;

            var DispParams = ParseSubblock(Input, ref i);
            if (DispParams[0] == "TRANSVERSE_DISP")
            {
                DisperserArrayH[LayerIndex] = new Normal(0.0, Math.Sqrt(2 * Convert.ToDouble(DispParams[1]) * StreamlineDelta));
                DisperserArrayV[LayerIndex] = new Normal(0.0, Math.Sqrt(2 * Convert.ToDouble(DispParams[2]) * StreamlineDelta));
            }
            else
            {
                DisperserArrayH[LayerIndex] = new Normal(0.0, 0.0);
                DisperserArrayV[LayerIndex] = new Normal(0.0, 0.0);
            }

            var ASParams = ParseSubblock(Input, ref i);
            switch (ASParams[0])
            {
                case "PARETO":
                    var Beta = ParseToDouble(ASParams[1]);
                    AdvectiveSubordinatorArray[LayerIndex] = new AdvectiveSubordinatorPareto(Beta);
                    break;

                case "TPL":
                    var T2T1Ratio = ParseToDouble(ASParams[1]);
                    Beta = ParseToDouble(ASParams[2]);
                    AdvectiveSubordinatorArray[LayerIndex] = new AdvectiveSubordinatorTPL(T2T1Ratio, Beta);
                    break;

                case "LOGNORMAL":
                    var Sigma = ParseToDouble(ASParams[1]);
                    AdvectiveSubordinatorArray[LayerIndex] = new AdvectiveSubordinatorLognormal(Sigma);
                    break;

                case "ADE":
                    var LongitudinalDispersivity = ParseToDouble(ASParams[1]);
                    AdvectiveSubordinatorArray[LayerIndex] = new AdvectiveSubordinatorADE(LongitudinalDispersivity);
                    break;

                case "NONE":
                    AdvectiveSubordinatorArray[LayerIndex] = new SubordinatorNone();
                    break;

                default:
                    throw new Exception("Unknown advective heterogeneity model: " + ASParams[0]);
            }

            var DSParams = ParseSubblock(Input, ref i);
            double Lambda;
            switch (DSParams[0])
            {
                case "TPL":
                    Lambda = ParseToDouble(DSParams[1]);
                    var T1 = ParseToDouble(DSParams[2]);
                    var T2 = ParseToDouble(DSParams[3]);
                    var Beta = ParseToDouble(DSParams[4]);
                    MIMTSubordinatorArray[LayerIndex] = new MIMTSubordinatorTPL(Lambda, T1, T2, Beta);
                    break;

                case "EXPONENTIAL":
                    Lambda = ParseToDouble(DSParams[1]);
                    var Mu = ParseToDouble(DSParams[2]);
                    MIMTSubordinatorArray[LayerIndex] = new MIMTSubordinatorExponential(Lambda, Mu);
                    break;

                case "NONE":
                    MIMTSubordinatorArray[LayerIndex] = new SubordinatorNone();
                    break;

                default:
                    throw new Exception("Unknown MIMT model: " + DSParams[0]);
            }
        }

        void ParseDomainBlock(in string[] Input, int FirstLineIndex)
        {
            for (int LayerIndex = 0; LayerIndex < NumCellsZ; LayerIndex++)
            {
                ParseLayerBlock(Input, FirstLineIndex, LayerIndex);
            }

        }

        void ParseSpeciesBlock(in string[] Input, int FirstLineIndex, int LastLineIndex)
        {
            for (int i = FirstLineIndex; i <= LastLineIndex; i++)
            {
                string Species = Input[i].Split(Delimiters, REE)[0];
                SpeciesList.Add(Species);
                DecayDictionary.Add(Species, new DecayRules());
            }
        }

        void ParseDecayBlock(in string[] Input, int FirstLineIndex, int LastLineIndex)
        {
            int i = FirstLineIndex;
            do
            {
                List<List<string>> Nuggets = ParseSubblockWithTuples(Input, ref i);
                string SourceSpecies = Nuggets[0][0];
                if (!SpeciesList.Contains(SourceSpecies))
                {
                    throw new Exception("Decay rule contains unknown species");
                }
                double Rate = ParseToDouble(Nuggets[1][0]);

                var DestSpecies = new List<string>();
                var DestMoles = new List<int>();
                for (int DestIdx = 2; DestIdx < Nuggets.Count; DestIdx++)
                { 
                    DestSpecies.Add(Nuggets[DestIdx][0]);
                    if (Nuggets[DestIdx].Count > 1) DestMoles.Add(Convert.ToInt32(Nuggets[DestIdx][1]));
                    else DestMoles.Add(1);
                }
                DecayDictionary[SourceSpecies].Add(new DecayRule(Rate, DestSpecies.ToArray(), DestMoles.ToArray()));

            } while (i <= LastLineIndex);
        }

        void ParseMIMTBlock(in string[] Input, int FirstLineIndex, int LastLineIndex)
        {
            int i = FirstLineIndex;
            do
            {
                List<string> Nuggets = ParseSubblock(Input, ref i);
                string Species = Nuggets[0];
                double ImmobilizationRateScaler = ParseToDouble(Nuggets[1]);
                double RemobilizationRateScaler = ParseToDouble(Nuggets[2]);

                MIMTScalingDictionary.Add(Species, new double[2] { ImmobilizationRateScaler, RemobilizationRateScaler });
            } while (i <= LastLineIndex);
        }

        void EncodeDefaultMIMTBehaviour()
        {
            //Add generic MIMT behaviour for species that did not have any defined in the marshal file 
            bool NoExplicitMIMTBehaviour;
            foreach (String Species in SpeciesList)
            {
                NoExplicitMIMTBehaviour = true;

                foreach (String Key in MIMTScalingDictionary.Keys)
                {
                    if (Key == Species)
                    {
                        NoExplicitMIMTBehaviour = false;
                        break;
                    }
                }
                if (NoExplicitMIMTBehaviour)
                {
                    MIMTScalingDictionary.Add(Species, new double[2] { 1.0, 1.0 });
                }
            }
        }

        static IVolumeOrSurface ParseSourceRegion(List<string> VolumeParams)
        {
            IVolumeOrSurface SourceRegion;
            switch (VolumeParams[0])
            {
                case "BOX":
                    var XMin = ParseToDouble(VolumeParams[1]);
                    var XMax = ParseToDouble(VolumeParams[2]);
                    var YMin = ParseToDouble(VolumeParams[3]);
                    var YMax = ParseToDouble(VolumeParams[4]);
                    var ZMin = ParseToDouble(VolumeParams[5]);
                    var ZMax = ParseToDouble(VolumeParams[6]);
                    SourceRegion = new Box(XMin, XMax, YMin, YMax, ZMin, ZMax);
                    break;

                case "CYLINDER":
                    var CXMid = ParseToDouble(VolumeParams[1]);
                    var CYMid = ParseToDouble(VolumeParams[2]);
                    var CZMin = ParseToDouble(VolumeParams[3]);
                    var CRadius = ParseToDouble(VolumeParams[4]);
                    var CHeight = ParseToDouble(VolumeParams[5]);
                    SourceRegion = new Cylinder(CXMid, CYMid, CZMin, CRadius, CHeight);
                    break;

                case "TUBE":
                    var TXMid = ParseToDouble(VolumeParams[1]);
                    var TYMid = ParseToDouble(VolumeParams[2]);
                    var TZMin = ParseToDouble(VolumeParams[3]);
                    var TRadius = ParseToDouble(VolumeParams[4]);
                    var THeight = ParseToDouble(VolumeParams[5]);
                    SourceRegion = new Tube(TXMid, TYMid, TZMin, TRadius, THeight);
                    break;

                case "SPHERE":
                    var SXMid = ParseToDouble(VolumeParams[1]);
                    var SYMid = ParseToDouble(VolumeParams[2]);
                    var SZMid = ParseToDouble(VolumeParams[3]);
                    var SRadius = ParseToDouble(VolumeParams[4]);
                    SourceRegion = new Sphere(SXMid, SYMid, SZMid, SRadius);
                    break;

                default:
                    throw new Exception("Unknown source zone type: " + VolumeParams[0]);
            }
            return SourceRegion;
        }

        void ParseSourceBlock(in string[] Input, int FirstLineIndex, int LastLineIndex)
        {
            //Console.WriteLine("FLI = {0}, LLI = {1}", FirstLineIndex,LastLineIndex);
            int i = FirstLineIndex;
            var NumParticles = Convert.ToDouble(Input[i++].Split(Delimiters, REE)[0]);
            
            var ReleaseParams = ParseSubblock(Input, ref i);

            string WeightScheme = Input[i++].Split(Delimiters, REE)[0].ToUpper();
            bool FluxWeighted = WeightScheme switch
            {
                "FLUX_WEIGHTED" => true,
                "UNIFORMLY_WEIGHTED" => false,
                _ => throw new Exception("Unknown weighting scheme.")
            };

            var VolumeParams = ParseSubblock(Input, ref i);
            var SourceRegion = ParseSourceRegion(VolumeParams);

            //Console.WriteLine("FLI = {0}, i = {1}, LLI = {2}", FirstLineIndex, i, LastLineIndex);
            //If a species is declared
            string Species = "Default";
            if (i <= LastLineIndex) 
            {
                var SpeciesParams = ParseSubblock(Input, ref i);
                Species = SpeciesParams[1];
            }

            Source NewSource;
            switch (ReleaseParams[0])
            {
                case "INSTANT":
                    var ReleaseTime = Convert.ToDouble(ReleaseParams[1]);
                    NewSource = new PulseSource(NumParticles, SourceRegion, ReleaseTime, FluxWeighted, this, Species);
                    break;

                case "CONTINUOUS":
                    var StartTime = Convert.ToDouble(ReleaseParams[1]);
                    var EndTime = Convert.ToDouble(ReleaseParams[2]);
                    NewSource = new ContinuousSource(NumParticles, SourceRegion, StartTime, EndTime, FluxWeighted, this, Species);
                    break;

                default:
                    throw new Exception("Unknown source zone type: " + VolumeParams[0]);
            }
            Sources.Add(NewSource);
        }

        static (string, double[], double[]) ParseConcentrationSubblock(in string[] Input, ref int CurrentLineIndex)
        {
            List<List<string>> EpochInfo = ParseSubblockWithTuples(Input, ref CurrentLineIndex);

            string Type = EpochInfo[0][0];
            EpochInfo.RemoveAt(0);

            List<double> EpochStartTimes = new();
            List<double> EpochConcentrations = new();

            foreach(List<string> EpochDatum in EpochInfo)
            { 
                EpochStartTimes.Add(Convert.ToDouble(EpochDatum[0]));
                EpochConcentrations.Add(Convert.ToDouble(EpochDatum[1]));
            }
            return (Type, EpochStartTimes.ToArray(), EpochConcentrations.ToArray());
        }

        void ParseMolarSourceBlock(in string[] Input, int FirstLineIndex)
        {
            int i = FirstLineIndex;

            (var Type, var EpochStartTimes, var EpochValues) = ParseConcentrationSubblock(Input, ref i);
            var EndTime = Convert.ToDouble(ParseSubblock(Input, ref i)[1]);
            var SourceRegion = ParseSourceRegion(ParseSubblock(Input, ref i));
            string Species = ParseSubblock(Input, ref i)[1]; 

            switch (Type)
            {
                case "FIXED_CONC":
                    var FCSource = new MolarConcentrationSource(EpochStartTimes, EpochValues, EndTime, SourceRegion, this, Species);
                    Sources.Add(FCSource);
                    MolarSourceEvents.Add(new SourceEvent(FCSource, Species));
                    break;

                case "FIXED_RATE":
                    var FRSource = new MolarRateSource(EpochStartTimes, EpochValues, EndTime, SourceRegion, this, Species);
                    Sources.Add(FRSource);
                    break;

                default:
                    throw new Exception("Uhnknown source zone type: " + Type);
            }
        }

        void ParsePrecipitationBlock(in string[] Input, int FirstLineIndex)
        {
            int i = FirstLineIndex;

            List<List<string>> EpochInfo = ParseSubblockWithTuples(Input, ref i);

            EpochInfo.RemoveAt(0);

            List<double> EpochStartTimesList = new();
            List<double> EpochConcentrationsList = new();

            foreach (List<string> EpochDatum in EpochInfo)
            {
                EpochStartTimesList.Add(Convert.ToDouble(EpochDatum[0]));    //Time step for steady state condition makes discrepancy between TimeStepBoundArray and EpochStartTime
                EpochConcentrationsList.Add(Convert.ToDouble(EpochDatum[1]));
            }
            var EndTime = Convert.ToDouble(ParseSubblock(Input, ref i)[1]);
            var VolumeParams = ParseSubblockWithTuples(Input, ref i);
            VolumeParams.RemoveAt(0);
            var xMin = Convert.ToDouble(VolumeParams[0][0]);
            var xMax = Convert.ToDouble(VolumeParams[1][0]);
            var yMin = Convert.ToDouble(VolumeParams[0][1]);
            var yMax = Convert.ToDouble(VolumeParams[1][1]);
            //Console.WriteLine("PPS");

            string Species = ParseSubblock(Input, ref i)[1];

            for (i=0; i<TimeStepBoundArray.Length; i++)
            {
                //Console.WriteLine("i = {0}, TimeBoundArray[i] = {1}",i, TimeStepBoundArray[i]);
                //Console.WriteLine("EpochStartTimeList = {0}, EpochConcList = {1}",EpochStartTimesList.Count,EpochConcentrationsList.Count);

                if (TimeStepBoundArray[i] < EndTime)
                {
                    if (!EpochStartTimesList.Contains(TimeStepBoundArray[i]))
                    {
                        int ListCount = EpochConcentrationsList.Count;
                        for (int j = 0; j < ListCount; j++)
                        {
                            if (TimeStepBoundArray[i] >= EpochStartTimesList[j])
                            {
                                //Console.WriteLine("i ={0}, j = {1}", i, j);
                                try
                                {
                                    if (TimeStepBoundArray[i] < EpochStartTimesList[j + 1])
                                    {
                                        EpochStartTimesList.Insert(j + 1, TimeStepBoundArray[i]);
                                        EpochConcentrationsList.Insert(j + 1, EpochConcentrationsList[j]);
                                        break;
                                    }
                                    if (TimeStepBoundArray[i] > EpochStartTimesList[^1])
                                    {
                                        EpochStartTimesList.Add(TimeStepBoundArray[i]);
                                        EpochConcentrationsList.Add(EpochConcentrationsList[j]);
                                        break;
                                    }
                                }
                                catch (IndexOutOfRangeException)
                                {
                                    if (TimeStepBoundArray[i] < TimeStepBoundArray[^1])
                                    {
                                        EpochStartTimesList.Insert(j, TimeStepBoundArray[i]);
                                        EpochConcentrationsList.Insert(j, EpochConcentrationsList[j]);
                                        break;
                                    }
                                }
                            }

                        }
                    }
                }
            }
            double[] EpochStartTimes = EpochStartTimesList.ToArray();
            double[] EpochValues = EpochConcentrationsList.ToArray();
            //Console.WriteLine("PPS");
            //Console.WriteLine("Length of EpochStartTimes = {0}", EpochStartTimes.Length);

            TopSurface SourceRegion = new (xMin, yMin, xMax, yMax, EpochStartTimes, this);
            //Console.WriteLine("SourceRegion done");

            var PTSource = new PrecipitationSource(EpochStartTimes, EpochValues, EndTime, SourceRegion, this, Species);
            //Console.WriteLine("PPS");

            PrecipitationSources.Add(PTSource);
        }

        void ParseProfileBlock(in string[] Input, int FirstLineIndex, int LastLineIndex)
        {
            for (int i = FirstLineIndex; i <= LastLineIndex; i ++)
            {
                double TriggerTime = Convert.ToDouble(Input[i].Split(Delimiters, REE)[0]);
                OutputEvents.Add(new ProfileEvent(TriggerTime, this));
            }
        }

        void ParseBreakthroughBlock(in string[] Input, int FirstLineIndex, int LastLineIndex)
        {
            int i = FirstLineIndex;
            do  
            {
                List<string> BTParams = ParseSubblock(Input, ref i);
                string Direction = BTParams[^1].ToUpper();
                double[] ShapeParams;

                //Tube breakthrough
                switch(BTParams[0])
                {
                    case "WELL":
                        ShapeParams = new double[4];
                        for (int Idx = 0; Idx < 4; Idx++) ShapeParams[Idx] = Convert.ToDouble(BTParams[Idx + 1]);

                        OutputEvents.Add(new WellBreakthroughEvent(ShapeParams, this));
                        break;

                    case "TUBE":
                        ShapeParams = new double[5];
                        for (int Idx = 0; Idx < 5; Idx++) ShapeParams[Idx] = Convert.ToDouble(BTParams[Idx + 1]);
                        
                        switch (Direction)
                        {
                            case "IN":
                                OutputEvents.Add(new TubeBreakthroughInEvent(ShapeParams));
                                break;
                            case "OUT":
                                OutputEvents.Add(new TubeBreakthroughOutEvent(ShapeParams));
                                break;
                            case "EITHER":
                                OutputEvents.Add(new TubeBreakthroughEitherEvent(ShapeParams));
                                break;
                            default:
                                throw new Exception("Unknown breakthrough direction flag: " + Direction);
                        }
                        break;

                    case "PLANE":
                        ShapeParams = new double[4];
                        for (int Idx = 0; Idx < 4; Idx++) ShapeParams[Idx] = Convert.ToDouble(BTParams[Idx + 1]);

                        switch (Direction)
                        {
                            case "IN":
                                OutputEvents.Add(new PlaneBreakthroughInEvent(ShapeParams));
                                break;
                            case "OUT":
                                OutputEvents.Add(new PlaneBreakthroughOutEvent(ShapeParams));
                                break;
                            case "EITHER":
                                OutputEvents.Add(new PlaneBreakthroughEitherEvent(ShapeParams));
                                break;
                            default:
                                throw new Exception("Unknown breakthrough direction flag: " + Direction);
                        }
                        break;

                    default:
                        throw new Exception("Unknown breakthrough geometry: " + BTParams[0]);
                }
            } while (i <= LastLineIndex);
        }
    }
}
﻿using System;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.Distributions;
using System.Linq;
using System.Diagnostics;

namespace Aurora
{
    abstract class Source
    {
        protected readonly double NumParticles;
        public readonly IVolumeOrSurface SourceRegion;
        protected readonly Simulation Parent;
        protected string Species;

        public Source(double NumParticles, IVolumeOrSurface SourceRegion, Simulation Parent, string Species = "Default")
        {
            this.NumParticles = NumParticles;
            this.SourceRegion = SourceRegion;
            this.Parent = Parent;
            this.Species = Species;
        }

        public abstract void InitializeParticlePositions();

        protected Vector<double> Candidate(Vector<double> LastVector)
        {
            return SourceRegion.RandomLocation();
        }
    }

    class PulseSource : Source
    {
        readonly double ReleaseTime;
        readonly ILocationSampler Sampler;

        public PulseSource(double NumParticles, IVolumeOrSurface SourceRegion, double ReleaseTime, bool FluxWeighted, Simulation Parent, string Species = "Default")
            : base(NumParticles, SourceRegion, Parent, Species)
        {
            this.ReleaseTime = ReleaseTime;

            if (FluxWeighted)
            {
                Sampler = new ArbitrarySampler(Candidate(Vector<double>.Build.Dense(new double[3] { 0, 0, 0 })),
                new ArbitrarySampler.Proposal(Candidate), new ArbitrarySampler.ProbabilityMass(Mass));
            }
            else
            {
                Sampler = (ILocationSampler)SourceRegion;
            }
        }

        private double Mass(Vector<double> Location)
        {
            var CellLocation = Parent.GetCellIndexes(Location);
            return Parent.GetVelocity(CellLocation, ReleaseTime, out _).L2Norm();
        }

        protected void InitializeParticlePositionsFromFunction(Func<double> ReleaseTimeFunc, string Species)
        {
            for (int i = 0; i < NumParticles; i++)
            {
                double ReleaseTime = ReleaseTimeFunc();
                Vector<double> Location = Sampler.Sample();
                Particle NextParticle = new(Location, ReleaseTime, Program.Simulation, Species);
                Program.Simulation.ParticleQueue.Enqueue(NextParticle);
            }
        }

        public override void InitializeParticlePositions()
        {
            InitializeParticlePositionsFromFunction(() => ReleaseTime, Species);
        }
    }

    class ContinuousSource : Source
    {
        readonly double StartTime;
        readonly double EndTime;

        delegate double MassDelegate(Vector<double> CurrentLocation);
        readonly MassDelegate[] MassDelegateArray;

        readonly ILocationSampler[] SamplerArray;


        public ContinuousSource(double NumParticles, IVolumeOrSurface SourceRegion, double StartTime, double EndTime,
            bool FluxWeighted, Simulation Parent, string Species = "Default") : base(NumParticles, SourceRegion, Parent, Species)
        {
            this.StartTime = StartTime;
            this.EndTime = EndTime;

            SamplerArray = new ILocationSampler[Parent.NumTimeSteps];
            MassDelegateArray = new MassDelegate[Parent.NumTimeSteps];

            for (int i = Parent.GetTimeStepIndex(StartTime); i <= Parent.GetTimeStepIndex(EndTime); i++)
            {
                if (FluxWeighted)
                {
                    MassDelegateArray[i] = new MassDelegate((Vector<double> CurrentLocation)
                        => Parent.GetVelocity(CurrentLocation, Parent.TimeStepBoundArray[i], out int TimeStepIndex).L2Norm());

                    SamplerArray[i] = new ArbitrarySampler(Candidate(Vector<double>.Build.Dense(new double[3] { 0, 0, 0 })),
                    new ArbitrarySampler.Proposal(Candidate), new ArbitrarySampler.ProbabilityMass(MassDelegateArray[i]));
                }
                else
                {
                    SamplerArray[i] = (ILocationSampler)SourceRegion;
                }
            }
        }

        protected void InitializeParticlePositionsFromFunction(Func<double> ReleaseTimeFunc, string Species)
        {
            for (int i = 0; i < NumParticles; i++)
            {
                double ReleaseTime = ReleaseTimeFunc();
                Vector<double> Location = SamplerArray[Parent.GetTimeStepIndex(ReleaseTime)].Sample();
                Particle NextParticle = new(Location, ReleaseTime, Program.Simulation, Species);
                Program.Simulation.ParticleQueue.Enqueue(NextParticle);
            }
        }

        public override void InitializeParticlePositions()
        {
            InitializeParticlePositionsFromFunction(() => ContinuousUniform.Sample(StartTime, EndTime), Species);
        }

    }

    class MolarConcentrationSource : Source
    {
        const int ParticlesAtPeakEpoch = 50;
        public readonly double[] EpochStartTimes;
        protected double[] EpochNumParticles;
        public double MolesPerParticle;
        public readonly double EndTime;

        readonly ILocationSampler Sampler;

        public MolarConcentrationSource(double[] EpochStartTimes, double[] EpochConcentrations, double EndTime, IVolumeOrSurface SourceRegion,
            Simulation Parent, string Species = "Default") : base(0, SourceRegion, Parent, Species)
        {
            if (Parent.MolesPerParticle is null)
                throw new Exception("Moles per particle must be defined in the MAIN block of the marshal file for MOLAR_SOURCE to be used");

            this.EpochStartTimes = EpochStartTimes;
            this.EndTime = EndTime;
            this.Species = Species;

            //Set so the maximum concentration epoch is represented by ParticlesAtPeakEpoch particles unless the particle amplification factor
            // is over one million, or Parent.MolesPerParticle causes it to have more particles
            double MaxConcentration = EpochConcentrations.Max();
            MolesPerParticle = Math.Min(MaxConcentration * SourceRegion.GetVolume() / ParticlesAtPeakEpoch, (double) Parent.MolesPerParticle);
            MolesPerParticle = Math.Max(MolesPerParticle, (double) Parent.MolesPerParticle / 1.0e7);

            EpochNumParticles = new double[EpochConcentrations.Length];
            int Idx = 0;
            foreach (double Concentration in EpochConcentrations)
            {
                var NumParticles = Convert.ToInt32(Concentration * SourceRegion.GetVolume() / MolesPerParticle);
                EpochNumParticles[Idx++] = NumParticles;
            }

            Sampler = (ILocationSampler)SourceRegion;
        }

        public int GetEpoch(double Time)
        {
            for (int EpochIdx = 1; EpochIdx < EpochStartTimes.Length; EpochIdx++)
            {
                if (EpochStartTimes[EpochIdx] > Time)
                    return EpochIdx - 1;
            }
            return EpochStartTimes.Length - 1;
        }

        public override void InitializeParticlePositions()
        {
            for (int EpochIdx = 0; EpochIdx < EpochStartTimes.Length; EpochIdx++)
            {
                for (int i = 0; i < EpochNumParticles[EpochIdx]; i++)
                {
                    double ReleaseTime = EpochStartTimes[EpochIdx];
                    Vector<double> Location = Sampler.Sample();
                    Particle NextParticle = new(Location, ReleaseTime, Program.Simulation, Species, this);
                    Program.Simulation.ParticleQueue.Enqueue(NextParticle);
                }
            }
        }

        public void ReplaceParticle(double ReleaseTime)
        {
            Vector<double> Location = Sampler.Sample();
            Particle NewParticle = new(Location, ReleaseTime, Program.Simulation, Species, this);
            Program.Simulation.ParticleQueue.Enqueue(NewParticle);
        }
    }

    delegate double MassDelegate(Vector<double> CurrentLocation);

    class PrecipitationSource
    {
        public readonly double[] EpochStartTimes;
        protected int[] EpochNumParticles;
        public readonly double EndTime;
        public string Species;
        public TopSurface SourceRegion;
        protected readonly Simulation Parent;

        readonly ILocationSampler[] SamplerArray;

        public PrecipitationSource(double[] EpochStartTimes, double[] EpochConcentrations, double EndTime, TopSurface SourceRegion,
            Simulation Parent, string Species = "Default")
        {
            this.EpochStartTimes = EpochStartTimes;
            this.EndTime = EndTime;
            this.Species = Species;
            this.SourceRegion = SourceRegion;
            this.Parent = Parent;
            if (Parent.MolesPerParticle is null)
                throw new Exception("Moles per particle must be defined in the MAIN block of the marshal file for MOLAR_SOURCE to be used");

            var LayerBottom = SourceRegion.LayerBottom;
            var WaterTable = SourceRegion.WaterTable;
            var MassDelegateArray = new MassDelegate[EpochStartTimes.Length];
            double[] MolesPerEpoch = new double[EpochStartTimes.Length];   
            EpochNumParticles = new int[EpochStartTimes.Length];
            SamplerArray = new ILocationSampler[EpochStartTimes.Length];

            for (int t = 0; t < EpochStartTimes.Length; t++)
            {
                int TimeStepIdx = Parent.GetTimeStepIndex(EpochStartTimes[t]);
                for (int i = 0; i < Parent.RechargeRate.GetLength(0); i++)
                    for (int j = 0; j < Parent.RechargeRate.GetLength(1); j++)
                    {
                        MolesPerEpoch[t] = MolesPerEpoch[t] + EpochConcentrations[t] * Parent.RechargeRate[i, j, Parent.GetLayerIndex(i, j, WaterTable[i, j, t]), TimeStepIdx];
                    }
            }
            for (int t = 0; t < EpochNumParticles.Length; t++)
            {
                try
                {
                    EpochNumParticles[t] = Convert.ToInt32(MolesPerEpoch[t] * (EpochStartTimes[t + 1] - EpochStartTimes[t]) / Parent.MolesPerParticle);
                }catch(IndexOutOfRangeException)
                {
                    EpochNumParticles[t] = Convert.ToInt32(MolesPerEpoch[t] * (EndTime - EpochStartTimes[t]) / Parent.MolesPerParticle);
                }
                MassDelegateArray[t] = new MassDelegate((Vector<double> CurrentLocation)
                    =>
                {
                return Parent.GetRechargeRate(CurrentLocation, EpochStartTimes[(int)CurrentLocation[3]]);
                });
                SamplerArray[t] = new ArbitrarySampler(Candidate(Vector<double>.Build.Dense(new double[4] { 0, 0, 0, t })), new ArbitrarySampler.Proposal(Candidate), new ArbitrarySampler.ProbabilityMass(MassDelegateArray[t]));
            }
        }
        
        public Vector<double> Candidate(Vector<double> SeedVector)
        {
            int EpochTimestepIndex = (int)SeedVector[3];
            return SourceRegion.RandomLocation(EpochTimestepIndex);
        }
        public void InitializeParticlePositions()
        {
            for (int EpochIdx = 0; EpochIdx < EpochStartTimes.Length; EpochIdx++)
            {
                for (int k = 0; k < EpochNumParticles[EpochIdx]; k++)
                {
                    double ReleaseTime = EpochStartTimes[EpochIdx];
                    try
                    {
                        ReleaseTime = EpochStartTimes[EpochIdx] + ContinuousUniform.Sample(0, 1) * (EpochStartTimes[EpochIdx + 1] - EpochStartTimes[EpochIdx]);
                    }catch (IndexOutOfRangeException)
                    {
                        ReleaseTime = EpochStartTimes[EpochIdx] + ContinuousUniform.Sample(0, 1) * (EndTime - EpochStartTimes[EpochIdx]);
                    }
                    Vector<double> Location = SamplerArray[EpochIdx].Sample();
                    Particle NextParticle = new(Vector<double>.Build.Dense(new double[3] { Location[0], Location[1], Location[2] }), ReleaseTime, Program.Simulation, Species);
                    Program.Simulation.ParticleQueue.Enqueue(NextParticle);
                }
            }
        }
    }
    class MolarRateSource : Source
	{
        public readonly double[] EpochStartTimes;
        protected readonly double[] EpochNumParticles;
        public readonly double EndTime;

        readonly ILocationSampler[] SamplerArray;

        public MolarRateSource(double[] EpochStartTimes, double[] EpochRates, double EndTime, IVolumeOrSurface SourceRegion,
            Simulation Parent, string Species = "Default") : base(0, SourceRegion, Parent, Species)
        {
            if (Parent.MolesPerParticle is null)
                throw new Exception("Moles per particle must be defined in MAIN block for MOLAR_SOURCE to be used");

            this.EpochStartTimes = EpochStartTimes;
            this.EndTime = EndTime;
            this.Species = Species;

            SamplerArray = new ILocationSampler[Parent.NumTimeSteps];

            EpochNumParticles = new double[EpochRates.Length];
            int Idx = 0;
            foreach (double Rate in EpochRates)
            {
				try
				{
                    EpochNumParticles[Idx] = Convert.ToInt32(Rate * (EpochStartTimes[Idx+1]-EpochStartTimes[Idx])/ Parent.MolesPerParticle);
                }
                catch (IndexOutOfRangeException)
				{
                    EpochNumParticles[Idx] = Convert.ToInt32(Rate * (EndTime - EpochStartTimes[Idx]) / Parent.MolesPerParticle);
                }
                Idx += 1;

                for (int i = Parent.GetTimeStepIndex(EpochStartTimes[0]); i <= Parent.GetTimeStepIndex(EndTime); i++)
                {
                    MassDelegate MyDelegate = new MassDelegate((Vector<double> CurrentLocation)
                       =>
                    {
                        Vector<double> Velocity = Parent.GetVelocity(CurrentLocation, Parent.TimeStepBoundArray[i], out int TimeStepIndex);

                        double Mass = Velocity.DotProduct(SourceRegion.OutwardNormal(CurrentLocation));

                        return Math.Max(Mass, 0.0);
                    });

                    SamplerArray[i] = new ArbitrarySampler(Candidate(Vector<double>.Build.Dense(new double[3] { 0, 0, 0 })),
                    new ArbitrarySampler.Proposal(Candidate), new ArbitrarySampler.ProbabilityMass(MyDelegate));
                }
            }
        }

        public override void InitializeParticlePositions()
        {
            for (int EpochIdx = 0; EpochIdx < EpochStartTimes.Length; EpochIdx++)
            {
                for (int i = 0; i < EpochNumParticles[EpochIdx]; i++)
                {
                    double ReleaseTime = EpochStartTimes[EpochIdx];
                    Vector<double> Location = SamplerArray[Parent.GetTimeStepIndex(ReleaseTime)].Sample();
                    Particle NextParticle = new(Location, ReleaseTime, Program.Simulation, Species);
                    Program.Simulation.ParticleQueue.Enqueue(NextParticle);
                }
            }
        }
    }
}

﻿using MathNet.Numerics.Distributions;

namespace Aurora
{
    abstract class Subordinator
    {
        public abstract double SubordinationMap(double OpertationalTimeDelta, Particle Particle);
    }

    class SubordinatorNone : Subordinator
    {
        public override double SubordinationMap(double OpertationalTimeDelta, Particle Particle)
        {
            return OpertationalTimeDelta;
        }
    }

    class AdvectiveSubordinatorADE : Subordinator
    {
        readonly double LongitudinalDispersivity;

        public AdvectiveSubordinatorADE(double LongitudinalDispersivity)
        {
            this.LongitudinalDispersivity = LongitudinalDispersivity;
        }

        public override double SubordinationMap(double OpertationalTimeDelta, Particle Particle)
        {
            double MovementTimeForUnitOperationalTime
                = InverseGaussian.Sample(1.0, Particle.Parent.StreamlineDelta / 2 / LongitudinalDispersivity);
            return OpertationalTimeDelta * MovementTimeForUnitOperationalTime;
        }
    }

    class AdvectiveSubordinatorPareto : Subordinator
    {
        readonly double Beta;

        public AdvectiveSubordinatorPareto(double Beta)
        {
            this.Beta = Beta;
        }

        public override double SubordinationMap(double OpertationalTimeDelta, Particle Particle)
        {
            double MovementTimeForUnitOperationalTime
                = Pareto.Sample((Beta-1)/ Beta, Beta);
            return OpertationalTimeDelta * MovementTimeForUnitOperationalTime;
        }
    }

    class AdvectiveSubordinatorTPL : Subordinator
    {
        readonly TPLSampler Sampler;

        public AdvectiveSubordinatorTPL(double T2T1Ratio, double Beta)
        {
            Sampler = new TPLSampler(T2T1Ratio, Beta, true);
        }

        public override double SubordinationMap(double OpertationalTimeDelta, Particle Particle)
        {
            double MovementTimeForUnitOperationalTime
                = Sampler.Sample();
            return OpertationalTimeDelta * MovementTimeForUnitOperationalTime;
        }
    }

    class AdvectiveSubordinatorLognormal : Subordinator
    {
        readonly double Sigma;

        public AdvectiveSubordinatorLognormal(double Sigma)
        {
            this.Sigma = Sigma;
        }

        public override double SubordinationMap(double OpertationalTimeDelta, Particle Particle)
        {
            double MovementTimeForUnitOperationalTime
                = LogNormal.Sample(-Sigma * Sigma / 2, Sigma);
            return OpertationalTimeDelta * MovementTimeForUnitOperationalTime;
        }
    }

    abstract class MIMTSubordinator : Subordinator
    {
        readonly double Lambda;

        public MIMTSubordinator(double Lambda)
        {
            this.Lambda = Lambda;
        }

        protected int GetNumberOfTrapEvents(double OperationalTimeDelta)
        {
            return Poisson.Sample(Lambda * OperationalTimeDelta);
        }
    }

    class MIMTSubordinatorExponential : MIMTSubordinator
    {
        readonly double Mu;

        public MIMTSubordinatorExponential(double Lambda, double Mu): base(Lambda)
        {
            this.Mu = Mu;
        }

        public override double SubordinationMap(double OpertationalTimeDelta, Particle Particle)
        {
            double TrapTime = 0;

            double ImmobilizationScaler = Particle.Parent.MIMTScalingDictionary[Particle.Species][0];
            double RemobilizationScaler = Particle.Parent.MIMTScalingDictionary[Particle.Species][1];

            for (int i = 0; i < GetNumberOfTrapEvents(OpertationalTimeDelta * ImmobilizationScaler); i++)
                TrapTime += Exponential.Sample(Mu) * RemobilizationScaler;

            return OpertationalTimeDelta + TrapTime;
        }
    }

    class MIMTSubordinatorTPL : MIMTSubordinator
    {
        readonly TPLSampler Sampler;

        public MIMTSubordinatorTPL(double Lambda, double T1, double T2, double Beta) : base(Lambda)
        {
            Sampler = new TPLSampler(T1, T2, Beta, true);
        }

        public override double SubordinationMap(double OpertationalTimeDelta, Particle Particle)
        {
            double TrapTime = 0;

            double ImmobilizationScaler = Particle.Parent.MIMTScalingDictionary[Particle.Species][0];
            double RemobilizationScaler = Particle.Parent.MIMTScalingDictionary[Particle.Species][1];

            for (int i = 0; i < GetNumberOfTrapEvents(OpertationalTimeDelta * ImmobilizationScaler); i++)
                TrapTime += Sampler.Sample() * RemobilizationScaler;

            return OpertationalTimeDelta + TrapTime;
        }
    }
}

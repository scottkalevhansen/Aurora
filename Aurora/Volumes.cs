﻿using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.Distributions;
using System;

namespace Aurora
{
    class Box : IVolumeOrSurface, ILocationSampler
    {
        public readonly double[] Min;
        public readonly double[] Max;
        public readonly double Volume;

        public Box(double XMin, double XMax, double YMin, double YMax, double ZMin, double ZMax)
        {
            Min = new[] { XMin, YMin, ZMin };
            Max = new[] { XMax, YMax, ZMax };
            Volume = (XMax - XMin) * (YMax - YMin) * (ZMax - ZMin);
        }

        public bool IsInside(Vector<double> Location)
        {
            bool Inside = true;
            for (int DimIdx = 0; DimIdx < 3; DimIdx++)
            {
                Inside &= Location[DimIdx] > Min[DimIdx] & Location[DimIdx] < Max[DimIdx];
            }
            return Inside;
        }

        public double GetVolume()
        {
            return Volume;
        }

        public Vector<double> RandomLocation()
        {
            var LocationArray = new double[3];
            for (int DimIdx = 0; DimIdx < 3; DimIdx++)
            {
                LocationArray[DimIdx] = ContinuousUniform.Sample(Min[DimIdx], Max[DimIdx]);
            }
            return Vector<double>.Build.Dense(LocationArray);
        }


        public virtual Vector<double> RandomSurfaceLocation()
        {
            var RNG = new Random();
            var Face = RNG.Next(6);

            var LocationArray = new double[3];
            for (int DimIdx = 0; DimIdx < 3; DimIdx++)
            {
                LocationArray[DimIdx] = ContinuousUniform.Sample(Min[DimIdx], Max[DimIdx]);
            }

            switch (Face)
            {
                case 0: //XMin face
                    return Vector<double>.Build.Dense(new double[] { Min[0], LocationArray[1], LocationArray[2] });
                case 1:
                    return Vector<double>.Build.Dense(new double[] { Max[0], LocationArray[1], LocationArray[2] });
                case 2: //YMin face
                    return Vector<double>.Build.Dense(new double[] { LocationArray[0], Min[1], LocationArray[2] });
                case 3:
                    return Vector<double>.Build.Dense(new double[] { LocationArray[0], Max[1], LocationArray[2] });
                case 4: //XMin face
                    return Vector<double>.Build.Dense(new double[] { LocationArray[0], LocationArray[1], Min[2] });
                default:
                    return Vector<double>.Build.Dense(new double[] { LocationArray[0], LocationArray[1], Max[2] });
            }
        }

        public Vector<double> OutwardNormal(Vector<double> Location)
        {
            var Normal = Vector<double>.Build.Dense(new double[] { -1, 0, 0 });
            double NearestDistance = Math.Abs(Location[0] - Min[0]);
            double CandidateDistance;

            CandidateDistance = Math.Abs(Location[0] - Max[0]);
            if (CandidateDistance < NearestDistance)
            {
                NearestDistance = CandidateDistance;
                Normal = Vector<double>.Build.Dense(new double[] { 1, 0, 0 });
            }

            CandidateDistance = Math.Abs(Location[1] - Min[1]);
            if (CandidateDistance < NearestDistance)
            {
                NearestDistance = CandidateDistance;
                Normal = Vector<double>.Build.Dense(new double[] { 0, -1, 0 });
            }

            CandidateDistance = Math.Abs(Location[1] - Max[1]);
            if (CandidateDistance < NearestDistance)
            {
                NearestDistance = CandidateDistance;
                Normal = Vector<double>.Build.Dense(new double[] { 0, 1, 0 });
            }

            CandidateDistance = Math.Abs(Location[2] - Min[2]);
            if (CandidateDistance < NearestDistance)
            {
                NearestDistance = CandidateDistance;
                Normal = Vector<double>.Build.Dense(new double[] { 0, 0, -1 });
            }

            CandidateDistance = Math.Abs(Location[2] - Max[2]);
            if (CandidateDistance < NearestDistance)
            {
                NearestDistance = CandidateDistance;
                Normal = Vector<double>.Build.Dense(new double[] { 0, 0, 1 });
            }

            return Normal;
        }

        public Vector<double> Sample()
        {
            return RandomLocation();
        }
    }

    class Cylinder : IVolumeOrSurface, ILocationSampler
    {
        public readonly double[] BottomCentre;
        public double Height, Radius;
        public double Volume;

        public Cylinder(double XMid, double YMid, double ZMin, double Radius, double Height)
        {
            BottomCentre = new[] { XMid, YMid, ZMin };
            this.Radius = Radius;
            this.Height = Height;

            Volume = Math.PI * Math.Pow(Radius, 2) * Height;
        }

        public bool IsInside(Vector<double> Location)
        {
            bool Inside = false;
            if (Location[2] > BottomCentre[2] && Location[2] < BottomCentre[2] + Height)
            {
                if (Math.Pow(Location[0] - BottomCentre[0], 2) + Math.Pow(Location[1] - BottomCentre[1], 2) < Math.Pow(Radius, 2))
                {
                    Inside = true;
                }
            }
            return Inside;
        }

        public double GetVolume()
        {
            return Volume;
        }

        public virtual Vector<double> RandomLocation()
        {
            var LocationArray = new double[3];
            var R = ContinuousUniform.Sample(0, Radius);
            var Theta = ContinuousUniform.Sample(0, 2 * Math.PI);

            LocationArray[0] = BottomCentre[0] + R * Math.Cos(Theta);
            LocationArray[1] = BottomCentre[1] + R * Math.Sin(Theta);
            LocationArray[2] = ContinuousUniform.Sample(BottomCentre[2], BottomCentre[2] + Height);

            return Vector<double>.Build.Dense(LocationArray);
        }

        public virtual Vector<double> RandomSurfaceLocation()
        {
            var LocationArray = new double[3];
            var Theta = ContinuousUniform.Sample(0, 2 * Math.PI);

            LocationArray[0] = BottomCentre[0] + Radius * Math.Cos(Theta);
            LocationArray[1] = BottomCentre[1] + Radius * Math.Sin(Theta);
            LocationArray[2] = ContinuousUniform.Sample(BottomCentre[2], BottomCentre[2] + Height);

            return Vector<double>.Build.Dense(LocationArray);
        }

        public Vector<double> OutwardNormal(Vector<double> Location)
        {
            var ProjectedCentre = Vector<double>.Build.Dense(new double[] { BottomCentre[0], BottomCentre[1], Location[2] });
            var Normal = Location - ProjectedCentre;

            return Normal / Normal.L2Norm();
        }

        public Vector<double> Sample()
        {
            return RandomLocation();
        }

    }

    class Tube : Cylinder
    {
        public Tube(double XMid, double YMid, double ZMin, double Radius, double Height) : base(XMid, YMid, ZMin, Radius, Height)
        {

        }

        public override Vector<double> RandomLocation()
        {
            var LocationArray = new double[3];
            var Theta = ContinuousUniform.Sample(0, 2 * Math.PI);

            LocationArray[0] = BottomCentre[0] + Radius * Math.Cos(Theta);
            LocationArray[1] = BottomCentre[1] + Radius * Math.Sin(Theta);
            LocationArray[2] = ContinuousUniform.Sample(BottomCentre[2], BottomCentre[2] + Height);

            return Vector<double>.Build.Dense(LocationArray);
        }
    }

    class Sphere : IVolumeOrSurface, ILocationSampler
    {
        public readonly double[] Centre;
        public double Radius;
        public double Volume;

        public Sphere(double XMid, double YMid, double ZMid, double Radius)
        {
            Centre = new[] { XMid, YMid, ZMid };
            this.Radius = Radius;
            this.Volume = Math.PI * Math.Pow(Radius, 3) * 4 / 3;
        }

        public bool IsInside(Vector<double> Location)
        {
            bool Inside = false;
            if (Math.Pow(Location[0] - Centre[0], 2) + Math.Pow(Location[1] - Centre[1], 2)
                + Math.Pow(Location[2] - Centre[2], 2) < Math.Pow(Radius, 2))
            {
                Inside = true;
            }
            return Inside;
        }

        public double GetVolume()
        {
            return this.Volume;
        }

        public Vector<double> RandomLocation()
        {
            var LocationArray = new double[3];
            var R = ContinuousUniform.Sample(0, Radius);
            var Theta = ContinuousUniform.Sample(0, 2 * Math.PI);
            var Phi = ContinuousUniform.Sample(0, 2 * Math.PI);

            LocationArray[0] = Centre[0] + R * Math.Cos(Phi) * Math.Cos(Theta);
            LocationArray[1] = Centre[1] + R * Math.Cos(Phi) * Math.Sin(Theta);
            LocationArray[2] = Centre[2] + R * Math.Sin(Phi);

            return Vector<double>.Build.Dense(LocationArray);
        }

        public Vector<double> RandomSurfaceLocation()
        {
            var LocationArray = new double[3];
            var R = ContinuousUniform.Sample(0, Radius);
            var Theta = ContinuousUniform.Sample(0, 2 * Math.PI);
            var Phi = ContinuousUniform.Sample(0, 2 * Math.PI);

            LocationArray[0] = Centre[0] + R * Math.Cos(Phi) * Math.Cos(Theta);
            LocationArray[1] = Centre[1] + R * Math.Cos(Phi) * Math.Sin(Theta);
            LocationArray[2] = Centre[2] + R * Math.Sin(Phi);

            return Vector<double>.Build.Dense(LocationArray);
        }

        public Vector<double> OutwardNormal(Vector<double> Location)
        {
            var Normal = Location - Vector<double>.Build.Dense(Centre);

            return Normal / Normal.L2Norm();
        }

        public Vector<double> Sample()
        {
            return RandomLocation();
        }

    }

    class TopSurface
    {
        public PositionData LeftLowerIndex;
        public PositionData RightUpperIndex;
        public double[,,] WaterTable;
        public double[,,] LayerBottom;
        protected readonly Simulation Parent;

        public TopSurface(double XGlobalMin, double YGlobalMin, double XGlobalMax, double YGlobalMax, double[] EpochStartTimes, Simulation Parent)
        {
            Vector<double> LeftLowerCorner = Vector<double>.Build.Dense(new double[3] { XGlobalMin, YGlobalMin, 0 });
            Vector<double> RightUpperCorner = Vector<double>.Build.Dense(new double[3] { XGlobalMax, YGlobalMax, 0 });
            this.Parent = Parent;

            LeftLowerIndex = Parent.GetCellIndexes(LeftLowerCorner);
            RightUpperIndex = Parent.GetCellIndexes(RightUpperCorner);

            this.WaterTable = new double[RightUpperIndex.XCellIndex - LeftLowerIndex.XCellIndex + 1, RightUpperIndex.YCellIndex - LeftLowerIndex.YCellIndex + 1, EpochStartTimes.Length];
            this.LayerBottom = new double[RightUpperIndex.XCellIndex - LeftLowerIndex.XCellIndex + 1, RightUpperIndex.YCellIndex - LeftLowerIndex.YCellIndex + 1, EpochStartTimes.Length];

            for (int t = 0; t < EpochStartTimes.Length; t++)
            {
                for (int i = 0; i < WaterTable.GetLength(0); i++)
                    for (int j = 0; j < WaterTable.GetLength(1); j++)
                    {
                        this.WaterTable[i, j, t] = Parent.GetHead(LeftLowerIndex.XCellIndex + i, LeftLowerIndex.YCellIndex + j, EpochStartTimes[t]);
                        this.LayerBottom[i, j, t] = Parent.ZBoundArray[LeftLowerIndex.XCellIndex + i, LeftLowerIndex.YCellIndex + j, Parent.GetLayerIndex(LeftLowerIndex.XCellIndex + i, LeftLowerIndex.YCellIndex + j, WaterTable[i,j,t] )];
                    }
            }
        }
        public Vector<double> RandomLocation(int EpochIdx)
        {
            double XLocation = ContinuousUniform.Sample(Parent.XBoundArray[LeftLowerIndex.XCellIndex], Parent.XBoundArray[RightUpperIndex.XCellIndex + 1]);
            double YLocation = ContinuousUniform.Sample(Parent.YBoundArray[LeftLowerIndex.XCellIndex], Parent.YBoundArray[RightUpperIndex.YCellIndex + 1]);
            int XIdx = Parent.GetXIndex(XLocation);
            int YIdx = Parent.GetYIndex(YLocation);
            var LocationArray = new double[4] { XLocation, YLocation, ContinuousUniform.Sample(LayerBottom[XIdx, YIdx, EpochIdx], WaterTable[XIdx, YIdx, EpochIdx]), EpochIdx };
            return Vector<double>.Build.Dense(LocationArray);
        }
    }
}

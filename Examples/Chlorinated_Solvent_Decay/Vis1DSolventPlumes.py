from sklearn.neighbors import KernelDensity
import numpy as np
import matplotlib.pyplot as mpl

def proc_file(filename):
    with open(filename, 'r') as f:
        lines = f.readlines()

    time = float(lines.pop(0).split()[0])

    d_locs = {}

    for line in lines:
        x, _, _, s = line.split()
        if s in d_locs.keys():
            d_locs[s].append(float(x))
        else:
            d_locs[s] = [float(x)]

    return time, d_locs

MAXPROIDX = 4
DIR = r"C:\Users\Scott\Documents\Aurora\Examples\Chlorinated Solvent Decay" + "\\"
COLOURLIST = ['r', 'g', 'b', 'k']
STYLELIST = ['solid', 'dotted', 'dashed', 'dashdot']
XAXIS = np.linspace(0,500, num=100)[:, np.newaxis]

t_list  = []
d_list = []

for idx in range (1, MAXPROIDX+1):
    t, d = proc_file(DIR+str(idx)+'.pro')
    t_list.append(t)
    d_list.append(d)

species_list = list({key for d in d_list for key in d.keys()})
print(species_list)
print(t_list)

#compute total particles
total_particles = 0
for species in species_list:
    try:
        total_particles += len(d_list[0][species])
    except:
        pass

#compute relative weights
weights = []
for tidx, t in enumerate(t_list):
    weights.append(dict())
    for species in species_list:
        if species in d_list[tidx].keys():
            weights[tidx][species] = len(d_list[tidx][species])/total_particles
        else:
            weights[tidx][species] = 0

mpl.figure()
for sidx, species in enumerate(species_list):
    for didx, d in enumerate(d_list):
        if species in d.keys():
            print(species, t_list[didx], len(d[species]),min(d[species]),max(d[species]))
            
            data = np.array(d[species], dtype=np.float64)[:,np.newaxis]
            kde = KernelDensity(kernel='gaussian', bandwidth=10).fit(data)
            log_dens = kde.score_samples(XAXIS) 
            mpl.plot(XAXIS[:,0], np.exp(log_dens)*weights[didx][species], COLOURLIST[sidx], ls=STYLELIST[didx], 
                lw=2, label="%s t=%.1E"%(species, t_list[didx]))

mpl.ylabel("Relative concentration")
mpl.xlabel("Distance [m]")
mpl.legend()
mpl.show()
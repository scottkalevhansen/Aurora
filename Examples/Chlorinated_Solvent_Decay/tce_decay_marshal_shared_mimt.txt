MAIN
    tce_decay.dis      	MODFLOW geometry discretization file name
    ASSUME_SATURATED
    GW_CHART_FILES
        tce_decay.f2        GW-Chart right flux file name
        NIL 		        GW-Chart front flux file name
        NIL                 GW-Chart bottom flux file name
    ESB
    1.0					Spatial length of each step along streamline [L]
    2e8 				Maximum time of simulation [T]
    AUTO_GRID_OFFSET
END

DOMAIN
    0.25				Assumed porosity
    NONE	    		Sub-grid transverse dispersivity [L]
    ADE -> 2.5
    EXPONENTIAL -> 4e-4 5e-4
END

SPECIES
    TCE
    DCE
    VC
END

DECAY
    TCE -> 1.46e-8 DCE
    DCE -> 1.68e-8 VC
    VC -> 1.36e-8 Null
END

SOURCE BOX
    5000 				Number of particles to employ
    INSTANT -> 0.0      Release time
    UNIFORMLY_WEIGHTED
    BOX
        5   			Release box: Minimum X [L]
        6			    Release box: Maximum X [L]
        -100			Release box: Minimum Y [L]
        0			    Release box: Maximum Y [L]
        -10			    Release box: Minimum Z [L]
        0			    Release box: Maximum Z [L]
    ESB
    SPECIES -> TCE
END

BREAKTHROUGHS
    PLANE -> 1 0 0 495 EITHER
END

PROFILES			Times at which particle snapshots are desired
    5e6
    5e7						Time of snapshot [T]
    1.0e8
    2.0e8
END
MAIN
    Flow_Field.dis   MODFLOW geometry discretization file name
    ASSUME_SATURATED
    GW_CHART_FILES
        Flow_Field.f2    GW-Chart right flux file name
        Flow_Field.f3    GW-Chart front flux file name
        NONE             GW-Chart bottom flux file name (No flow through bottom, this is a 2D model)
    ESB
    1.0                     Spatial length of each step along streamline [L] (meters per cell)
    40000.0                   Maximum time of simulation, days
    AUTO_GRID_OFFSET
END

DOMAIN
    0.36                Assumed porosity
    TRANSVERSE_DISP -> 0.01 0
    None                Advective subordinator
    None                Diffusive subordinator
END

SOURCE
    1000                Number of particles to employ
    INSTANT -> 0        Time of release
    UNIFORMLY_WEIGHTED
    BOX
        5.0            Release box: Minimum X [L]
        55.0            Release box: Maximum X [L]
        5.0            Release box: Minimum Y [L]
        195           Release box: Maximum Y [L]
        -0.5            Release box: Minimum Z [L]
        -0.5            Release box: Maximum Z [L]
    ESB
END

BREAKTHROUGHS           Coefficients defining breakthrough plane: aX + bY +cZ = d
    PLANE -> 1 0 0 399.9 IN
END

PROFILES                Times at which particle snapshots are desired
   0.0
   500.0
   1000.0
END

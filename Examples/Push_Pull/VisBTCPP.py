# -*- coding: utf-8 -*-
"""
VisBTC.py

Created on Wed May 18 02:27:17 2016

@author: Scott
"""
from numpy import linspace
from scipy.stats import gaussian_kde
from pylab import ticklabel_format, plot, show, title, xlabel, ylabel
import pylab as plt

from matplotlib import rc
## for Palatino and other serif fonts use:
#rc('font',**{'family':'serif','serif':['Palatino']})
rc('text', usetex=True)

directory = r"C:\Users\Scott\Documents\Aurora\Examples\Push Pull"
filenames = ["times-a.txt","times-b.txt"]

fig, ax = plt.subplots()
ax.ticklabel_format(style='sci',axis='both')

secondsperday = 60*60*24

for filename in filenames:
    with open(directory + "\\" + filename,'r') as f:
        lines = f.readlines()

    tp = lines[0].split("#")[0].split("\t")
    #plottitle = "BTC at ax+by+cx=d, a=" + tp[0]+ " b=" + tp[1] + " c=" + tp[2]+ " d=" + tp[3]
    plottitle = "Empirical breakthrough curves at well"

    points0 = [float(x) for x in lines[1:]]
    points = [point for point in points0 if point > 86000]

    points.sort()
    kernel = gaussian_kde(points)
    #times = linspace(points[0],points[-1],100)
    times = linspace(0,6e5,100)
    density = kernel(times)
    ax.plot(times/secondsperday,density*secondsperday,linewidth=2.5)

ax.legend(['$\mu = 1 \cdot 10^{-5}$','$\mu = 5 \cdot 10^{-4}$'])

title(plottitle)
xlabel('Time [d]')
ylabel('Probability density [$\mathrm{d}^{-1}$]')
show()
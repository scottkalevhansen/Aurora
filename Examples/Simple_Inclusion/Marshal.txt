MAIN
    simplehetero.dis	MODFLOW geometry discretization file name
    ASSUME_SATURATED
    GW_CHART_FILES
        simplehetero.f2		GW-Chart right flux file name
        simplehetero.f3		GW-Chart front flux file name
        simplehetero.f4		GW-Chart bottom flux file name
    ESB
    0.1					Spatial length of each step along streamline [L]
    2.1e5				Maximum time of simulation [T]
    AUTO_GRID_OFFSET
END

DOMAIN
    0.25				Assumed porosity
    TRANSVERSE_DISP -> 1e-2 1e-2
    None				Advective subordinator
    None				Diffusive subordinator
END

SOURCE
    100					Number of particles to employ
    INSTANT -> 0.0      Time of release
	UNIFORMLY_WEIGHTED
	BOX
		0.0					Release box: Minimum X [L]
		4.0					Release box: Maximum X [L]
		-1.0				Release box: Minimum Y [L]
		0.0					Release box: Maximum Y [L]
		-10.0				Release box: Minimum Z [L]
		0.0	    			Release box: Maximum Z [L]
	ESB
END

SOURCE
    100					Number of particles to employ
    INSTANT -> 0.0      Time of release
	UNIFORMLY_WEIGHTED
    BOX -> 4.0  10.0  -1.0  0.0 -10.0  0.0
END

BREAKTHROUGHS		Coefficients defining breakthrough plane: aX + bY +cZ = d
    PLANE -> 0 1 0 -10 EITHER
    PLANE -> 0 1 0 -25 EITHER
END

PROFILES			Times at which particle snapshots are desired
    0.5e5						Time of snapshot [T]
    1.0e5						Time of snapshot [T]
    1.5e5
    2.0e5
END

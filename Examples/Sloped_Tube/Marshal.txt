MAIN
    slopetube.dis		MODFLOW geometry discretization file name
    ASSUME_SATURATED
    GW_CHART_FILES 
        slopetube.f2		GW-Chart right flux file name
        NIL			GW-Chart front flux file name
        NIL			GW-Chart bottom flux file name
    ESB
    0.1				    Spatial length of each step along streamline [L]
    5e4				    Maximum time of simulation [T]
    AUTO_GRID_OFFSET
END

LAYER 0
    0.25				Assumed porosity
    TRANSVERSE_DISP -> 1e-1 1e-1				
    None				Advective subordinator
    None				Diffusive subordinator
END

PROFILES			    Times at which particle snapshots are desired
    0.5e4
    1e4
    1.5e4
    2e4
    2.5e4
    3e4
END

SOURCE
    2000 				Number of particles to employ
    INSTANT -> 0.0      Release time
	UNIFORMLY_WEIGHTED
    BOX
        0				Release box: Minimum X [L]
        5		    	Release box: Maximum X [L]
        -10				Release box: Minimum Y [L]
        0   			Release box: Maximum Y [L]
        -15	    		Release box: Minimum Z [L]
        5		    	Release box: Maximum Z [L]
    ESB
END

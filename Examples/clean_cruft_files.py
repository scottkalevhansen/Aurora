from os import remove
from pathlib import Path

BADEXTENSIONS = ['archive','axml','bas','bat','bcf','bdn','btc','chd','end','-events.txt','fdn','ftl','gridmeta','gsf','lmt','log','lpf','lst', 'mnw2', 'mnwi', 'mpbas','mplst','mpn','mpsim','nam','oc','path','pcg','pro','_rate_array.txt','reference','strt','wel','wel_out']

for extension in BADEXTENSIONS:
    for path in Path('.').rglob('*'+extension):
        print('removing', path)
        path.unlink()
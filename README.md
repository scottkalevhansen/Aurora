![Aurora](./logo/aurora_wordmark_small.png)

## Welcome to the Aurora Git repository

Aurora is a particle tracker that works closely with MODFLOW-2005 and MODFLOW-2000. Unlike existing particle trackers such as MODPATH, which are limited to pure streamline tracing or to homogeneous velocity fields, Aurora can model small-scale Fickian dispersion, non-Fickian advection, multi- and single-rate kinetic mass transfer, diffusion into secondary porosity, and kinetic sorption. The combination of MODFLOW and Aurora enables a natural and practical approach to modelling solute transport at the field scale: deterministic, explicitly resolved flow information from MODFLOW is used directly while unresolved small-scale physical and chemical heterogeneity that may cause non-Fickian transport is captured by Aurora. Particle tracking solves many numerical problems inherent in Eulerian transport modelling, and Aurora is the first full-physics particle tracker for groundwater transport problems. It is also the first software, regardless of numerical approach, to model general non-Fickian subsurface transport.

## Version 1.5 :: May 2024
This version **eliminates dependency on external tools ModelMuse and GW-Chart**, and now reads water table height to **accurately determine flow velocities in partially saturated cells**.

**New features:**
* Now supports Windows, Linux, and macOS on x64 architecture.
* Directly reads cell-by-cell fluxes from MODFLOW's binary-format *.cbc file, eliminating the need for manually pre-processing this file with GW-Chart.
* Accurately determines velocities in partially saturated cells by directly determining water table location from the binary-format *.bhd file if provided. Transient conditions are supported.
* Eliminates reliance on ModelMuse's coordinate annotations in the *.dis file. Any or no preprocessor can now be used.
* New Monte Carlo algorithm for more accurate flux-weighted injection and travel time sampling.

## Version 1.4 :: June 2022
This version works with marshal files specified in **arbitrarily rotated and translated coordinate systems** and features a number of performance enhancements including **multi-threaded parallel operation**.

**New features:**
* Grid angle and offset of the lower left corner of the MODFLOW grid may be specified in the marshal file. The marshal file may thus be specified in terms of an arbitrarily rotated and offset coordinate system.
* Operation has been refactored to reduce memory footprint and to process particles in parallel, exploiting all cores on the target machine.
* Automatic rescaling and stochastic injection logic means that molar concentration sources of any strength can be used with any global moles-per-particle setting, without the worry that a source may not emit particles.
* Layer interface interpolation has been redesigned to employ triangular tessellations that are continuous under all conditions.
* Console user interface has been enhanced.


## Version 1.3 :: January 2022

This version contains a number of major enhancements to performance and capabilities, including **multi-species reactive transport** and **source zones with specified molar concentrations or injection rates** (previously Aurora only worked with raw particle injections).

**New features:**
* Specified molar concentration transient sources (a specified concentration history is maintained within user-specified sub-domains).
* Specified molar injection rate transient sources (flux-weighted and associated with downgradient faces of user-specified sub-domains).
* Improved handling of irregular layers; the smooth interpolation shown in ModelMuse is exactly replicated.
* Multi-species operation with arbitrary first-order decay networks and species-specific mobile-immobile behaviour.
* Separate vertical and horizontal transverse dispersion, based off of local velocity vector.
* Parallel operation for increased speed and new status monitor.
* Improved documentation and examples.

## Resources

- [Homepage](https://aurora-hub.gitlab.io)
- [User's guide](https://aurora-hub.gitlab.io/manual.html)
- [Download executable (and examples)](https://aurora-hub.itch.io/aurora)

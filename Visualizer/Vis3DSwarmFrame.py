# -*- coding: utf-8 -*-
"""
Vis3DSwarmFrame.py

Visualization codes for the particle location output files
Generates the following diagrams:
    - One 3D scatter plot showing all the particle locations
    - Three 2D heatmaps, showing depth-averaged concentration
    - One 1D distribution, showing concentration in the y-direction (implcitly 
    assumes that advection is in the +y-direction, but this is easy to change)

Created on Wed May 18 02:27:17 2016

@author: Scott
"""
from os import listdir
from re import split
from numpy import linspace, mgrid, meshgrid, ones, reshape, rot90, vstack, zeros
from scipy.stats import gaussian_kde
import matplotlib.pyplot as plt
from matplotlib.ticker import FormatStrFormatter
import mpl_toolkits.mplot3d.axes3d

directory = r"C:\Users\Scott\Documents\Aurora\Examples\Push Pull"
filename = "1.pro"

'''Obtain these from the .dis file. TO DO: parse that file automatically'''
x_delta_raw = ''' 5.000000000000E+000   5.000000000000E+000   5.000000000000E+000   5.000000000000E+000   2.500000000000E+000   2.500000000000E+000   1.666666666667E+000   1.666666666667E+000   1.666666666667E+000   2.500000000000E+000    2.500000000000E+000   5.000000000000E+000   5.000000000000E+000   5.000000000000E+000   5.000000000000E+000'''
y_delta_raw = ''' 5.000000000000E+000   5.000000000000E+000   5.000000000000E+000   5.000000000000E+000   2.500000000000E+000   2.500000000000E+000   1.666666666667E+000   1.666666666667E+000   1.666666666667E+000   2.500000000000E+000   2.500000000000E+000   5.000000000000E+000   5.000000000000E+000   5.000000000000E+000   5.000000000000E+000'''
z_raw = '''CONSTANT     0.000000000000E+000  # TOP
CONSTANT    -5.000000000000E+000  # BOTM, Upper Aquifer
CONSTANT    -6.000000000000E+000  # BOTM, Middle Aquifer
CONSTANT    -1.000000000000E+001  # BOTM, Lower Aquifer'''

x_deltas = [float(nugget) for nugget in x_delta_raw.replace('\t',' ').replace('\n',' ').split()]
xticks = [sum(x_deltas[0:i]) for i in range(len(x_deltas)+1)]
y_deltas = [float(nugget) for nugget in y_delta_raw.replace('\t',' ').replace('\n',' ').split()]
yticks = [-1*sum(y_deltas[0:i]) for i in range(len(y_deltas)+1)]
zticks = [float(list(filter(None, line.replace('\t',' ').split(' ')))[1]) for line in z_raw.split('\n')]


disfilename = [filename for filename in listdir(directory) if ".dis" in filename][0]
with open(directory + "\\" + disfilename,'r') as f:
    lines = f.readlines()
    for line in lines:
        if "Lower left" in line:
            nuggets = list(filter(None, split('[(,)]+',line)))
            xmin = float(nuggets[1])
            ymin = float(nuggets[2])
        if "Upper right" in line:
            nuggets = list(filter(None, split('[(,)]+',line)))
            xmax = float(nuggets[1])
            ymax = float(nuggets[2])

zmin = -10
zmax = 0

with open(directory + "\\" + filename,'r') as f:
    lines = f.readlines()

plottime = lines[0].split("#")[0].strip()
plottitle = "Plume snapshot at t= " + plottime
numpts = len(lines)-1

xs = zeros(numpts)
ys = zeros(numpts)
zs = zeros(numpts)

idx = 0
for l in lines[1:]:
    pts = [float(x) for x in l.split("\t") if len(x) > 1]
    xs[idx] = pts[0]
    ys[idx] = pts[1]
    zs[idx] = pts[2]
    idx += 1

#3D VIEW

fig = plt.figure(figsize=(10,10))
ax = fig.add_subplot(111, projection='3d')
ax.scatter(xs, ys, zs, s=5, color="darkgreen")

xx, yy = meshgrid(xticks, yticks)
for z in zticks:
    ax.plot_surface(xx, yy, z*ones((len(xticks),len(yticks))), color="goldenrod", alpha=0.05)

ax.tick_params(labelsize=9)
ax.set_xlabel('x [m]')
ax.set_xlim((xmin,xmax))
ax.set_xticks(xticks)
[l.set_visible(False) for (i,l) in enumerate(ax.xaxis.get_ticklabels()[0:-1]) if i % 3 != 0]
ax.xaxis.set_major_formatter(FormatStrFormatter('%.0f'))
ax.set_ylabel('y [m]')
ax.set_ylim((ymin,ymax))
ax.set_yticks(yticks)
[l.set_visible(False) for (i,l) in enumerate(ax.yaxis.get_ticklabels()[0:-1]) if i % 3 != 0]
ax.yaxis.set_major_formatter(FormatStrFormatter('%.0f'))
ax.set_zlabel('z [m]')
ax.set_zlim((zmin,zmax))
ax.set_zticks(zticks)
ax.set_aspect('auto','box')

plt.show()
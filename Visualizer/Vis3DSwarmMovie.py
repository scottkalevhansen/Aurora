# -*- coding: utf-8 -*-
"""
Vis3DSwarmMovie.py

Visualization codes for the particle location output files
Generates the following diagrams:
    - One 3D scatter plot showing all the particle locations
    - Three 2D heatmaps, showing depth-averaged concentration
    - One 1D distribution, showing concentration in the y-direction (implcitly 
    assumes that advection is in the +y-direction, but this is easy to change)

Created on Wed May 18 02:27:17 2016

@author: Scott
"""
from os import listdir
from re import split
from numpy import linspace, mgrid, reshape, rot90, vstack, zeros
from scipy.stats import gaussian_kde
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import mpl_toolkits.mplot3d.axes3d

secondsperday = 24*60*60
directory = r"..\Examples\Push Pull"

disfilename = [filename for filename in listdir(directory) if ".dis" in filename][0]
with open(directory + "\\" + disfilename,'r') as f:
    lines = f.readlines()
    for line in lines:
        if "Lower left" in line:
            nuggets = list(filter(None, split('[(,)]+',line)))
            xmin = float(nuggets[1])
            ymin = float(nuggets[2])
        if "Upper right" in line:
            nuggets = list(filter(None, split('[(,)]+',line)))
            xmax = float(nuggets[1])
            ymax = float(nuggets[2])

zmin = -10
zmax = 0

filenames = [filename for filename in listdir(directory) if ".pro" in filename]

print (filenames)

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.set_xlim((xmin,xmax))
ax.set_ylim((ymin,ymax))
ax.set_zlim((zmin,zmax))
ax.set_aspect('auto','box')

containers = list()

for filename in filenames:
    with open(directory + "\\" + filename,'r') as f:
        lines = f.readlines()

    plottime = str(float(lines[0].split("#")[0].strip())/secondsperday)
    plottitle = "Particle swarm at t= " + plottime + " days"
    numpts = len(lines)-1

    xs = zeros(numpts)
    ys = zeros(numpts)
    zs = zeros(numpts)

    idx = 0
    for l in [line for line in lines[1:] if len(line) > 1]:

        pts = [float(x) for x in l.split("\t") if len(x) > 1]
        xs[idx] = pts[0]
        ys[idx] = pts[1]
        zs[idx] = pts[2]
        
        idx += 1

    #3D VIEW

    #fig = plt.figure()
    artist = ax.scatter(xs, ys, zs, color="black")
    containers.append([artist])

ani = animation.ArtistAnimation(fig, containers, interval=500, blit=True, repeat_delay=2000)
ani.save(directory + "\\" + "particle_swarm_movie.gif")

plt.show()
# -*- coding: utf-8 -*-
"""
VisPlumeMapMovie.py

Created on Wed Dec 5 2018

@author: Scott Hansen
"""
from os import listdir
from re import split
import matplotlib.pyplot as plt
from matplotlib.animation import ArtistAnimation, PillowWriter
from types import SimpleNamespace
from VisPlumeMapFrame import generate_frame

#directory = r"C:\Burnell\aurora_simulation"
#backimagename = "map_crop.png"

directory = r"..\Examples\Simple_Inclusion"
domaindata = {'lowerleft' : [0, -30],
    'dimensions' : [10, 30],
    'angle' : 0, 
    'backimagename' : None,
    'timeperdisptimeunit': 24*60*60,
    'disptimeunit' : 'days'}

disfilename = [filename for filename in listdir(directory) if ".dis" in filename][0]

with open(directory + "\\" + disfilename,'r') as f:
    lines = f.readlines()
    for line in lines:
        if "Lower left" in line:
            nuggets = list(filter(None, split('[(,)]+',line)))
            xmin = float(nuggets[1])
            ymin = float(nuggets[2])
        if "Upper right" in line:
            nuggets = list(filter(None, split('[(,)]+',line)))
            xmax = float(nuggets[1])
            ymax = float(nuggets[2])
bounds = [xmin, xmax, ymin, ymax]
print(bounds)

files = [filename for filename in listdir(directory) if ".pro" in filename]
files.sort(key=lambda e: float(e.split('.')[0]))

containers = []

fig, ax = plt.subplots()

for filename in files:
    container = generate_frame(directory, filename, SimpleNamespace(**domaindata), figure=fig)
    containers.append(container)
    
ani = ArtistAnimation(fig, containers, interval=500, blit=False, repeat_delay=0)
writer = PillowWriter()
ani.save(directory + "\\" + "plume_map_movie.gif", writer=writer)

plt.xlabel('x')
plt.ylabel('y')
plt.show()